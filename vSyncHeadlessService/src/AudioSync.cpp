/*
 * AudioSync.cpp
 *
 *  Created on: 01 дек. 2014 г.
 *      Author: sergey
 */

#include <AudioSync.hpp>
#include <VBerry/ResponseAlbums.hpp>
#include <Settings.h>
#include <QDir>
#include <QTimer>
#include <VBerry/ResponseAudio.hpp>
#include <VBerry/AttachDownloader.hpp>

const QString AudioSync::folderPath = "shared/music/";

AudioSync::AudioSync(QObject *parent)
		: QObject(parent) {
	attachDownloader = new AttachDownloader(this);
	connect(attachDownloader, SIGNAL(downloadError(QString)), this, SLOT(onDownloadError(QString)));
	connect(attachDownloader, SIGNAL(downloadFinished(QString)), this, SLOT(onDownloadFinished(QString)));
	connect(attachDownloader, SIGNAL(allDone()), this, SLOT(onAllDone()));

	audioRequest = new RequestAudio(this);
	connect(audioRequest, SIGNAL(audioResponse(QList<ResponseAudio*>)), this, SLOT(onAudioResponse(QList<ResponseAudio*>)));
	connect(audioRequest, SIGNAL(errorResponse(int)), this, SLOT(onErrorResponse(int)));
	currentTrack = 1;
	totalTracks = 0;
	albumId = 0;
}

AudioSync::~AudioSync() {
	delete audioRequest;
	audioRequest = NULL;
	delete attachDownloader;
	attachDownloader = NULL;
}

void AudioSync::onErrorResponse(int code) {
	qDebug() << "Error when request audio, error code: " << code;
	emit errorSync();
}

void AudioSync::contiunueSync() {
	attachDownloader->continueDownload();
}

void AudioSync::stopSync() {
	attachDownloader->stopDownload();
}

void AudioSync::startSync() {
	if (attachDownloader->status() == AttachDownloader::InProgress) {
		return;
	} else if (attachDownloader->status() == AttachDownloader::Paused) {
		attachDownloader->continueDownload();
	} else if (attachDownloader->status() == AttachDownloader::Waiting) {
		QScopedPointer<ResponseAlbums> album(getCurrentAlbum());
		if (!album) {
			emit syncFinished();
			return;
		}

		albumId = Settings::instance()->albumId();
		albumPath = createAlbumFolder(album->title());
		album->deleteLater();

		if (!albumId || albumPath.length() == 0) {
			emit syncFinished();
			return;
		}
		audioRequest->getByAlbumId(albumId);
	}
}

void AudioSync::onAudioResponse(QList<ResponseAudio*> response) {
	totalTracks = response.length();
	emit progressChanged();

	for(int i = 0;i < response.length();i++) {
		QString destFile = getDestFileName(response.at(i));
		if (!QFile::exists(destFile)) {
			attachDownloader->appendToDownload(response.at(i)->getUrl(), destFile);
		}
		response.at(i)->deleteLater();
	}
	attachDownloader->download();
}

void AudioSync::onDownloadFinished(const QString &fileName) {
	currentTrack++;
	emit progressChanged();
	qDebug() << fileName;
}

void AudioSync::onDownloadError(const QString& message) {
	currentTrack++;
	emit progressChanged();
	qDebug() << message;
}

void AudioSync::onAllDone() {
	emit syncFinished();
}

QString AudioSync::getDestFileName(ResponseAudio* audio) {
	QString fileName = escapeName(audio->getArtist() + " - " + audio->getTitle());
	return albumPath + "/" + fileName + "." + QFileInfo(QUrl(audio->getUrl()).path()).suffix();
}

QString AudioSync::createAlbumFolder(const QString &albumName) {
	QString escapedAlbumName = escapeName(albumName);
	if (!QDir(folderPath + escapedAlbumName).exists()) {
		QDir dir;
		dir.mkpath(folderPath + escapedAlbumName);
	}

	return folderPath + escapedAlbumName;
}

QString AudioSync::escapeName(QString fileName) {
	return fileName.remove(QRegExp("[\\//:*\"?<>].")).simplified();
}

VBerry::ResponseAlbums* AudioSync::getCurrentAlbum() {
	QByteArray albumsCache = Settings::instance()->albumsCache();
	QDataStream in(&albumsCache, QIODevice::ReadOnly);

	while(!in.atEnd()) {
		ResponseAlbums * album = new ResponseAlbums();
		if (album->unserialize(&in)) {
			if (Settings::instance()->albumId() == album->albumId()) {
				return album;
			}
		}
	}

	return 0;
}

int AudioSync::getTotal() {
	return totalTracks;
}

int AudioSync::getCurrent() {
	return currentTrack;
}
