/*
 * AudioSync.hpp
 *
 *  Created on: 01 дек. 2014 г.
 *      Author: sergey
 */

#ifndef AUDIOSYNC_HPP_
#define AUDIOSYNC_HPP_

#include <QObject>
#include <VBerry/RequestAudio.hpp>
#include <VBerry/ResponseAlbums.hpp>
#include <VBerry/ResponseAudio.hpp>
#include <VBerry/AttachDownloader.hpp>

using namespace VBerry;

class AudioSync: public QObject {
	Q_OBJECT
public:
	AudioSync(QObject *parent = 0);
	virtual ~AudioSync();
	QString createAlbumFolder(const QString &albumName);
	VBerry::ResponseAlbums *getCurrentAlbum();
	static const QString folderPath;
	void startSync();
	void stopSync();
	void contiunueSync();
	int getTotal();
	int getCurrent();
signals:
	void progressChanged();
	void syncFinished();
	void errorSync();
protected slots:
	void onErrorResponse(int code);
	void onAudioResponse(QList<ResponseAudio*> response);
	void onDownloadFinished(const QString &file);
	void onDownloadError(const QString &message);
	void onAllDone();
protected:
	RequestAudio *audioRequest;
	QString getDestFileName(ResponseAudio* audio);
	QString escapeName(QString fileName);

	AttachDownloader *attachDownloader;
	QString albumPath;
	uint albumId;
	int totalTracks;
	int currentTrack;
};

#endif /* AUDIOSYNC_HPP_ */
