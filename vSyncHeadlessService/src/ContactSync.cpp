/*
 * ContactSync.cpp
 *
 *  Created on: 01 дек. 2014 г.
 *      Author: sergey
 */

#include <src/ContactSync.hpp>
#include <Phone/Contacts.hpp>
#include <Settings.h>

using namespace Phone;

ContactSync::ContactSync(QObject *parent)
		: QObject(parent) {
	friends = new RequestFriends();
	totalContacts = 0;
	currentContact = 1;
	connect(friends, SIGNAL(friendsResponse(const QList<ResponseFriend*>&)), this, SLOT(onFriendsResponse(const QList<ResponseFriend*>&)));
	connect(friends, SIGNAL(errorResponse(int)), this, SLOT(onErrorResponse(int)));
}

void ContactSync::startSync() {
	friends->getFriends();
}

void ContactSync::onErrorResponse(int code) {
	qDebug() << "Error when request audio, error code: " << code;
	emit errorSync();
}

ContactSync::~ContactSync() {
	friends->deleteLater();
};

void ContactSync::onSyncCompleated() {
	emit syncFinished();
}

void ContactSync::onFriendsResponse(const QList<ResponseFriend*> & response) {
	totalContacts = response.length();
	QScopedPointer<Contacts> c(new Contacts());
	for(int i=0;i<response.length();i++) {
		if (!Settings::instance()->isContactSynced(response.at(i)->getId())) {
			c->processContact(response.at(i));
		}
		response.at(i)->deleteLater();
		currentContact++;
		emit progressChanged();
	}
	c->deleteLater();
	emit syncFinished();
}

int ContactSync::getTotal() {
	return totalContacts;
}

int ContactSync::getCurrent() {
	return currentContact;
}
