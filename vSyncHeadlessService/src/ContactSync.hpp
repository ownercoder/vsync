/*
 * ContactSync.hpp
 *
 *  Created on: 01 дек. 2014 г.
 *      Author: sergey
 */

#ifndef CONTACTSYNC_HPP_
#define CONTACTSYNC_HPP_

#include <QObject>
#include <VBerry/ResponseFriend.hpp>
#include <VBerry/RequestFriends.hpp>

using namespace VBerry;

class ContactSync: public QObject {
	Q_OBJECT
public:
	explicit ContactSync(QObject *parent = 0);
	virtual ~ContactSync();
	void startSync();
	virtual void stopSync() {};
	virtual void contiunueSync() {};
	int getTotal();
	int getCurrent();
signals:
	void progressChanged();
	void syncFinished();
	void errorSync();
protected slots:
	void onFriendsResponse(const QList<ResponseFriend*> & response);
	void onSyncCompleated();
	void onErrorResponse(int code);
protected:
	RequestFriends *friends;
	QString getFileName(const QUrl &url);
	QString photoPath;
	int totalContacts;
	int currentContact;
};

#endif /* CONTACTSYNC_HPP_ */
