/*
 * Copyright (c) 2013-2014 BlackBerry Limited.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "service.hpp"

#include <bb/Application>

#include <QLocale>
#include <QTranslator>
#include <Settings.h>

using namespace bb;

class MyApplication: public Application {
public:
	MyApplication(int& argc, char** argv) :
		Application(argc, argv) {}
	virtual ~MyApplication() {};
	virtual bool notify(QObject* receiver, QEvent* event) {
		bool done = true;
		try {
			done = Application::notify(receiver, event);
		} catch (const std::exception& ex) {
			qCritical() << "Critical: " << ex.what();
		}
		return done;
	}
};

int main(int argc, char **argv)
{
	QCoreApplication::instance()->setApplicationName("vSync");
	QCoreApplication::instance()->setOrganizationName("Slytech");

	MyApplication app(argc, argv);

	// Initialize settings
	Settings::instance();

	// Create the Application UI object, this is where the main.qml file
	// is loaded and the application scene is set.
	Service::instance();

	// Enter the application main event loop.
	return Application::exec();
}
