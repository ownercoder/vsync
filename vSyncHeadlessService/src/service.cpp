/*
 * Copyright (c) 2013-2014 BlackBerry Limited.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "service.hpp"

#include <bb/Application>
#include <bb/platform/Notification>
#include <bb/system/InvokeManager>
#include <QFileSystemWatcher>
#include <Settings.h>
#include <QDateTime>

#include <tc/utils/ConnectionInfo.hpp>

using namespace bb::platform;
using namespace bb::system;
using namespace tc::utils;

Service *Service::m_Service = 0;

Service::Service() :
		QObject(),
		invokeManager(new InvokeManager(this)),
		connected(false) {

	invokeManager->connect(invokeManager, SIGNAL(invoked(const bb::system::InvokeRequest&)),
			this, SLOT(handleInvoke(const bb::system::InvokeRequest&)));

	settingsWatcher = new QFileSystemWatcher(this);
	settingsWatcher->addPath(Settings::instance()->fileName());
	bool ok = connect(settingsWatcher, SIGNAL(fileChanged(const QString&)), this, SLOT(onSettingChanged(const QString&)));
	Q_ASSERT(ok);

	connectionInfo = new ConnectionInfo();
	connect(connectionInfo, SIGNAL(connectedChanged(bool)), this, SLOT(onConnectionStateChanged(bool)));

	progress = new Progress(this);
	progress->setProgressStatus(Progress::NO_PROGRESS);
	progress->setProgressStart(0);

	audioSync = NULL;
	contactSync = NULL;

	startSyncWhenConnected = false;

	serviceCron = new QTimer(this);
	serviceCron->setInterval(43200 * 1000);
	connect(serviceCron, SIGNAL(timeout()), this, SLOT(cronStart()));

	if (Settings::instance()->syncActionState() == Settings::SYNC_CONTINUE) {
		qDebug() << "Cron timer start";
		serviceCron->start();
	}

	Notification::clearEffectsForAll();
	Notification::deleteAllFromInbox();
}

void Service::expiredToken() {
	qDebug() << "Send notify";
	InvokeRequest request;
	request.setTarget("com.slytech.vSync");

	notify.setTitle(tr("User action required"));
	notify.setBody(tr("VK login required"));
	notify.setInvokeRequest(request);
	notify.notify();
}

void Service::startContactSync() {
	if (!Settings::instance()->syncContacts() || !isConnected()) {
		return;
	}

	contactSync = new ContactSync(this);
	connect(contactSync, SIGNAL(syncFinished()), this, SLOT(onContactSyncFinished()));
	connect(contactSync, SIGNAL(progressChanged()), this, SLOT(updateProgress()));
	connect(contactSync, SIGNAL(errorSync()), this, SLOT(onErrorContactSync()));
	contactSync->startSync();
}

void Service::startAudioSync() {
	if (!Settings::instance()->syncAudio() || !isConnected()) {
		return;
	}

	audioSync = new AudioSync(this);
	connect(audioSync, SIGNAL(syncFinished()), this, SLOT(onAudioSyncFinished()));
	connect(audioSync, SIGNAL(progressChanged()), this, SLOT(updateProgress()));
	connect(audioSync, SIGNAL(errorSync()), this, SLOT(onErrorAudioSync()));
	audioSync->startSync();
}

void Service::onContactSyncFinished() {
	qDebug() << "Contact sync finished";
	delete contactSync;
	contactSync = NULL;
	updateProgress();
}

void Service::onAudioSyncFinished() {
	qDebug() << "Audio sync finished";
	delete audioSync;
	audioSync = NULL;
	updateProgress();
}

void Service::onConnectionStateChanged(bool status) {
	connected = status;
	updateConnectionStatus();
}

bool Service::isConnected() {
	return connected;
}

void Service::startAllSync() {
	if (progress->progressStatus() == Progress::SYNCING) {
		return;
	}

	if (Settings::instance()->token().length() == 0) {
		return;
	}
	startContactSync();
	startAudioSync();
}

void Service::updateConnectionStatus() {
	if (connectionInfo->interfaceType() == ConnectionInfo::Cellular && !Settings::instance()->syncViaMobileData()) {
		connected = false;
	} else {
		connected = connectionInfo->isConnected();
	}

	if (startSyncWhenConnected) {
		QTimer::singleShot(0, this, SLOT(startAllSync()));
	}
}

Service *Service::instance() {
	if (!m_Service) {
		m_Service = new Service();
	}

	return m_Service;
}

void Service::cronStart() {
	qDebug() << "Cron started";

	if (!isConnected()) {
		startSyncWhenConnected = true;
		serviceCron->stop();
		return;
	}

	if (Settings::instance()->token() == "") {
		return;
	}

	serviceCron->stop();
	startAllSync();
}

void Service::updateProgress() {
	int total = 0;
	int current = 0;

	if (audioSync == NULL && contactSync == NULL) {
		progress->setProgressStatus(Progress::NO_PROGRESS);
		progress->setLastSyncTimestamp(QDateTime::currentDateTime().toTime_t());
		if (Settings::instance()->syncActionState() != Settings::SYNC_PAUSE) {
			serviceCron->start();
		}
		progress->sync();
		return;
	}

	if (audioSync) {
		total += audioSync->getTotal();
		current += audioSync->getCurrent();
	}

	if (contactSync) {
		total += contactSync->getTotal();
		current += contactSync->getCurrent();
	}

	progress->setProgressEnd(total);
	progress->setProgress(current);
	progress->setProgressStatus(Progress::SYNCING);
	progress->sync();
}

void Service::handleInvoke(const bb::system::InvokeRequest & request) {
	qDebug() << request.action();
	if (request.action().contains(".SYNC_FORCE")) {
		QTimer::singleShot(0, this, SLOT(startAllSync()));
	} else if (request.action().contains(".SYNC_PAUSE") && progress->progressStatus() == Progress::SYNCING) {
		stopAllSync();
	} else if (request.action().contains(".SYNC_CONTINUE")) {
		continueAllSync();
	}
}

void Service::stopAllSync() {
	if (audioSync) audioSync->stopSync();
	if (contactSync) contactSync->stopSync();
}

void Service::continueAllSync() {
	if (Settings::instance()->syncAudio()) {
		if (audioSync) {
			audioSync->contiunueSync();
		}
	}

	if (Settings::instance()->syncContacts()) {
		if (contactSync) {
			contactSync->contiunueSync();
		}
	}
}

void Service::onSettingChanged(const QString &file) {
	Settings::instance()->reload();
	updateConnectionStatus();

	if (!isConnected()) {
		stopAllSync();
	}

	if (Settings::instance()->syncAudio() == false && audioSync != NULL) {
		audioSync->stopSync();
		audioSync->deleteLater();
		audioSync = NULL;
		updateProgress();
	}

	if (Settings::instance()->syncActionState() == Settings::SYNC_PAUSE && serviceCron->isActive()) {
		serviceCron->stop();
	} else if (Settings::instance()->syncActionState() == Settings::SYNC_CONTINUE && !serviceCron->isActive()) {
		serviceCron->start();
	}

	qDebug() << "File: " << file << " changed";
}

void Service::onErrorAudioSync() {
	delete audioSync;
	audioSync = NULL;
	updateProgress();
	expiredToken();
}

void Service::onErrorContactSync() {
	delete contactSync;
	contactSync = NULL;
	updateProgress();
	expiredToken();
}

