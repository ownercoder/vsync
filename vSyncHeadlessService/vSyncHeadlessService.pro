APP_NAME = vSyncHeadlessService

CONFIG += qt warn_on

INCLUDEPATH += $$PWD/src/

QT += core network
QT += sql declarative
QT += xml

include(config.pri)

LIBS += -lbbsystem  -lbbplatform -lbb -lbbdata -lbbpim
