/*
 * RequestAlbums.hpp
 *
 *  Created on: 24 нояб. 2014 г.
 *      Author: sergey
 */

#ifndef REQUESTALBUMS_HPP_
#define REQUESTALBUMS_HPP_

#include "OAuthConnection.h"
#include <VBerry/RequestAlbums.hpp>
#include <VBerry/ResponseAlbums.hpp>
#include <bb/data/JsonDataAccess>

namespace VBerry {

class RequestAlbums: public QObject {
	Q_OBJECT
public:
	RequestAlbums(QObject *parent = NULL);
	Q_INVOKABLE void getAlbums();
	Q_INVOKABLE void cancel();
Q_SIGNALS:
	void albumsResponse(const QList<ResponseAlbums*> & response);
	void errorResponse(int code);
protected Q_SLOTS:
	void onReplyFinished();
protected:
	QNetworkReply *currentReply;
	OAuthConnection *connection;
};

} /* namespace VBerry */

#endif /* REQUESTALBUMS_HPP_ */
