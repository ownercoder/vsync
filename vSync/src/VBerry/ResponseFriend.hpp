/*
 * ResponseFriend.hpp
 *
 *  Created on: 25 нояб. 2014 г.
 *      Author: sergey
 */

#ifndef RESPONSEFRIEND_HPP_
#define RESPONSEFRIEND_HPP_

#include <QObject>
#include <QUrl>
#include "SerializedBase.hpp"

namespace VBerry {

class ResponseFriend: public SerializedBase {
	Q_OBJECT
	Q_PROPERTY(uint id READ getId WRITE setId)
	Q_PROPERTY(QString firstName READ getFirstName WRITE setFirstName)
	Q_PROPERTY(QString lastName READ getLastName WRITE setLastName)
	Q_PROPERTY(QUrl photo READ getPhoto WRITE setPhoto)
	Q_PROPERTY(QUrl photo200 READ getPhoto200 WRITE setPhoto200)
	Q_PROPERTY(QString mobilePhone READ getMobilePhone WRITE setMobilePhone)
	Q_PROPERTY(QString homePhone READ getHomePhone WRITE setHomePhone)
	Q_PROPERTY(QString screenName READ getScreenName WRITE setScreenName)
public:
	ResponseFriend();
	virtual ~ResponseFriend() {};
	uint getId();
	QString getFirstName();
	QString getHomePhone();
	QString getLastName();
	QString getMobilePhone();
	QUrl getPhoto();
	QUrl getPhoto200();
	QString getScreenName();

public slots:
	void setPhoto(const QUrl& photo);
	void setHomePhone(const QString& homePhone);
	void setMobilePhone(const QString& mobilePhone);
	void setFirstName(const QString& firstName);
	void setLastName(const QString& lastName);
	void setId(const uint &id);
	void setScreenName(const QString& screenName);
	void setPhoto200(const QUrl& photo);

protected:
	uint _id;
	QString _firstName;
	QString _lastName;
	QUrl _photo;
	QString _mobilePhone;
	QString _homePhone;
	QString _screenName;
	QUrl _photo200;

};

} /* namespace VBerry */

#endif /* RESPONSEFRIEND_HPP_ */
