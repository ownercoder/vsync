/*
 * RequestFriends.hpp
 *
 *  Created on: 25 нояб. 2014 г.
 *      Author: sergey
 */

#ifndef REQUESTFRIENDS_HPP_
#define REQUESTFRIENDS_HPP_

#include <QObject>
#include <bb/data/JsonDataAccess>

#include <VBerry/ResponseFriend.hpp>
#include <VBerry/OAuthConnection.h>

namespace VBerry {

class RequestFriends: public QObject {
	Q_OBJECT
public:
	RequestFriends();
	virtual ~RequestFriends();
	Q_INVOKABLE void getFriends();
	Q_INVOKABLE void cancel();
signals:
	void friendsResponse(const QList<ResponseFriend*> & response);
	void errorResponse(int code);
protected slots:
	void onReplyFinished();
protected:
	OAuthConnection *connection;
	QNetworkReply *currentReply;
};

} /* namespace VBerry */

#endif /* REQUESTFRIENDS_HPP_ */
