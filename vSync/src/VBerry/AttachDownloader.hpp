/*
 * AttachDownloader.hpp
 *
 *  Created on: 29 нояб. 2014 г.
 *      Author: sergey
 */

#ifndef ATTACHDOWNLOADER_HPP_
#define ATTACHDOWNLOADER_HPP_

#include <QObject>
#include <QQueue>
#include <QFile>
#include <QNetworkAccessManager>
#include <QEventLoop>
#include <QNetworkReply>
#include <VBerry/OAuthConnection.h>

namespace VBerry {

class AttachDownloader: public QObject {
	Q_OBJECT
public:
	explicit AttachDownloader(QObject * parent = NULL);
	virtual ~AttachDownloader();
	enum Status {
		Waiting,
		InProgress,
		Paused,
	};
	Status status();
	void appendToDownload(const QString &sourceFile, const QString &destFile);
	bool download(const QString &sourceFile, const QString &destFile);
	void download();
	void stopDownload();
	void continueDownload();
signals:
	void downloadFinished(const QString &file);
	void downloadError(const QString &message);
	void allDone();
protected Q_SLOTS:
	void onReadyRead();
	void onFinished();
	void onError(QNetworkReply::NetworkError code);
	void onMetaDataChanged();
protected:
	bool deviceHasSufficientFileSpace(qint64 dataSize, QString destinationFileSystem);
	void startNextDownload();

	bool canDownload;
	QEventLoop loop;
	OAuthConnection * manager;
	QNetworkReply * currentDownload;
	QQueue<QPair<QString, QString> > downloadQueue;
	QFile *output;
	Status currentStatus;

};

} /* namespace VBerry */

#endif /* ATTACHDOWNLOADER_HPP_ */
