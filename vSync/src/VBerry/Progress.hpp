/*
 * Progress.hpp
 *
 *  Created on: 14 дек. 2014 г.
 *      Author: sergey
 */

#ifndef PROGRESS_HPP_
#define PROGRESS_HPP_

#include <QObject>
#include <QSettings>
#include <QFileSystemWatcher>

namespace VBerry {

class Progress: public QSettings {
	Q_OBJECT
public:
	Progress(QObject * parent = 0);
	virtual ~Progress();
	enum Status {
		NO_PROGRESS,
		SYNCING
	};

	QString getLastSyncFormated();
	void setLastSyncTimestamp(uint time);
	void setProgressStart(int value);
	void setProgressEnd(int value);
	void setProgress(int value);
	void setProgressStatus(VBerry::Progress::Status status);
	VBerry::Progress::Status progressStatus();
	int progressStart();
	int progressEnd();
	int progress();
signals:
	void progressChanged();
protected:
	QFileSystemWatcher *watcher;
protected slots:
	void onFileChanged(const QString& fileName);
private:
	static Progress *m_Instance;
};

} /* namespace VBerry */

#endif /* PROGRESS_HPP_ */
