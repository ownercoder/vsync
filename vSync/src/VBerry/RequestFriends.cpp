/*
 * RequestFriends.cpp
 *
 *  Created on: 25 нояб. 2014 г.
 *      Author: sergey
 */

#include <VBerry/RequestFriends.hpp>
#include <VBerry/ResponseFriend.hpp>
#include <QNetworkReply>

namespace VBerry {

RequestFriends::RequestFriends() {
	connection = new OAuthConnection();
	currentReply = 0;
}

RequestFriends::~RequestFriends() {
	connection->deleteLater();
}

void RequestFriends::getFriends() {
	QMap<QString, QString> params;
	params.insert("fields", "photo_max_orig,contacts");
	params.insert("name_case", "nom");
	currentReply = connection->requestApi("friends.get", params);
	connect(currentReply, SIGNAL(finished()), this, SLOT(onReplyFinished()));
}

void RequestFriends::onReplyFinished() {
	bb::data::JsonDataAccess jda;
	QByteArray data = currentReply->readAll();
//	qDebug() << data;
	QVariant friendsData = jda.loadFromBuffer(data);
	currentReply->deleteLater();
	if (jda.hasError()) {
		const bb::data::DataAccessError err = jda.error();
		const QString errorMsg = QString("Error converting JSON data: %1").arg(err.errorMessage());
		qDebug() << errorMsg;
		return;
	}

	QVariantList list = friendsData.value<QVariantMap>().value("response").value<QVariantList>();
	if (!list.length() && friendsData.value<QVariantMap>().value("error").value<QVariantMap>().contains("error_code")) {
		int error_code = friendsData.value<QVariantMap>().value("error").value<QVariantMap>().value("error_code").toInt();
		emit errorResponse(error_code);
		return;
	}
	QList<ResponseFriend*> friends;

	for(int i = 1; i < list.length(); i++) {
		ResponseFriend *contact = new ResponseFriend();
		contact->setId(list.at(i).toMap().value("user_id").toUInt());
		contact->setFirstName(list.at(i).toMap().value("first_name").toString());
		contact->setLastName(list.at(i).toMap().value("last_name").toString());
		contact->setPhoto(QUrl(list.at(i).toMap().value("photo_max_orig").toString()));
		contact->setMobilePhone(list.at(i).toMap().value("mobile_phone").toString());
		contact->setHomePhone(list.at(i).toMap().value("home_phone").toString());

		friends << contact;
	}

	emit friendsResponse(friends);
}

void RequestFriends::cancel() {
	currentReply->abort();
	currentReply->deleteLater();
}

} /* namespace VBerry */
