/*
 * ResponseFriend.cpp
 *
 *  Created on: 25 нояб. 2014 г.
 *      Author: sergey
 */

#include <VBerry/ResponseFriend.hpp>

namespace VBerry {

ResponseFriend::ResponseFriend() {
}

QString ResponseFriend::getLastName() {
	return _lastName;
}

QString ResponseFriend::getMobilePhone() {
	return _mobilePhone;
}

QUrl ResponseFriend::getPhoto() {
	return _photo;
}

void ResponseFriend::setPhoto(const QUrl& photo) {
	_photo = photo;
}

void ResponseFriend::setHomePhone(const QString& homePhone) {
	_homePhone = homePhone;
}

void ResponseFriend::setMobilePhone(const QString& mobilePhone) {
	_mobilePhone = mobilePhone;
}

void ResponseFriend::setFirstName(const QString& firstName) {
	_firstName = firstName;
}

void ResponseFriend::setLastName(const QString& lastName) {
	_lastName = lastName;
}

void ResponseFriend::setId(const uint& id) {
	_id = id;
}

QString ResponseFriend::getFirstName() {
	return _firstName;
}

uint ResponseFriend::getId() {
	return _id;
}

QString ResponseFriend::getHomePhone() {
	return _homePhone;
}

QString ResponseFriend::getScreenName() {
	return _screenName;
}

void ResponseFriend::setScreenName(const QString& screenName) {
	_screenName = screenName;
}

QUrl ResponseFriend::getPhoto200() {
	return _photo200;
}

void ResponseFriend::setPhoto200(const QUrl& photo) {
	_photo200 = photo;
}

} /* namespace VBerry */

