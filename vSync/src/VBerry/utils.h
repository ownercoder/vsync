/*
 * utils.h
 *
 *  Created on: 16 нояб. 2014 г.
 *      Author: sergey
 */

#ifndef UTILS_H_
#define UTILS_H_

#include <QtCore>

template<int N>
Q_INLINE_TEMPLATE QStringList flagsToStrList(int i, const char *(&strings)[N]) {
    QStringList list;
    for (int pos = 0; pos < N; pos++) {
        int flag = 1 << pos;
        if ((flag) & i)
            list.append(strings[pos]);
    }
    return list;
}


#endif /* UTILS_H_ */
