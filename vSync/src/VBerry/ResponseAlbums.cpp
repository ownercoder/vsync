/*
 * ResponseAlbums.cpp
 *
 *  Created on: 24 нояб. 2014 г.
 *      Author: sergey
 */

#include <VBerry/ResponseAlbums.hpp>

namespace VBerry {

ResponseAlbums::ResponseAlbums() {
}

QString ResponseAlbums::title() {
	return _title;
}

uint ResponseAlbums::ownerId() {
	return _ownerId;
}

uint ResponseAlbums::albumId() {
	return _albumId;
}

void ResponseAlbums::setTitle(const QString &value) {
	_title = value;
}

void ResponseAlbums::setOwnerId(const uint& value) {
	_ownerId = value;
}

void ResponseAlbums::setAlbumId(const uint& value) {
	_albumId = value;
}

} /* namespace VBerry */
