/*
 * SerializableClass.hpp
 *
 *  Created on: 25 нояб. 2014 г.
 *      Author: sergey
 */

#ifndef SERIALIZABLECLASS_HPP_
#define SERIALIZABLECLASS_HPP_

#include <QObject>
#include <QDataStream>

namespace VBerry {

class SerializedBase : public QObject {
	Q_OBJECT
public:
	explicit SerializedBase(QObject *parent = 0);
	bool serialize(QDataStream *dataStream);
	bool unserialize(QDataStream *dataStream);
};

} /* namespace VBerry */

#endif /* SERIALIZABLECLASS_HPP_ */
