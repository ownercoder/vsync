/*
 * Progress.cpp
 *
 *  Created on: 14 дек. 2014 г.
 *      Author: sergey
 */

#include <VBerry/Progress.hpp>
#include <QDebug>
#include <QStringList>
#include <QDateTime>

namespace VBerry {

Progress *Progress::m_Instance = 0;

Progress::Progress(QObject * parent) :
		QSettings("data/Settings/Slytech/progress.conf", QSettings::NativeFormat, parent) {
	if (!this->contains("status")) {
		this->setValue("status", Progress::NO_PROGRESS);
	}
	this->sync();

	//watcher = new QFileSystemWatcher();
	//watcher->addPath(this->fileName());
	//qDebug() << watcher->files();
	//connect(watcher, SIGNAL(fileChanged(const QString&)), this, SLOT(onFileChanged(const QString&)));
}

Progress::~Progress() {
	//watcher->deleteLater();
	this->deleteLater();
}

void Progress::setLastSyncTimestamp(uint time) {
	this->setValue("lastsync", time);
}

QString Progress::getLastSyncFormated() {
	qint64 currentTime = QDateTime::currentDateTime().toTime_t();
	qint64 lastSyncTime = this->value("lastsync", 0).toUInt();
	qint64 timeLeft = currentTime - lastSyncTime;

	if (lastSyncTime <= 0) {
		return tr("never");
	}

	if (timeLeft < 60 && timeLeft >= 0) {
		return tr("just now");
	} else if (timeLeft < 3600) {
		return tr("%1 minute(s) ago").arg(timeLeft/60);
	} else if (timeLeft < 86400) {
		return tr("%1 hour(s) ago").arg(timeLeft/60/60);
	} else {
		return tr("%1 day(s) ago").arg(timeLeft/60/60/60);
	}
}

void Progress::setProgressStart(int value) {
	this->setValue("start", value);
}

void Progress::setProgressEnd(int value) {
	this->setValue("end", value);
}

void Progress::setProgress(int value) {
	this->setValue("progress", value);
}

void Progress::setProgressStatus(Status status) {
	this->setValue("status", status);
}

void Progress::onFileChanged(const QString& fileName) {
	Q_UNUSED(fileName);

	emit progressChanged();
}

VBerry::Progress::Status Progress::progressStatus() {
	return static_cast<Status>(this->value("status").toInt());
}

int Progress::progressStart() {
	return this->value("start", 0).toInt();
}

int Progress::progressEnd() {
	return this->value("end", 0).toInt();
}

int Progress::progress() {
	return this->value("progress", 0).toInt();
}

} /* namespace VBerry */

