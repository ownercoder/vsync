/*
 * SerializableClass.cpp
 *
 *  Created on: 25 нояб. 2014 г.
 *      Author: sergey
 */

#include <VBerry/SerializedBase.hpp>
#include <QMetaObject>
#include <QMetaProperty>
#include <QDebug>

namespace VBerry {

SerializedBase::SerializedBase(QObject *parent) :
		QObject(parent) {
}

bool SerializedBase::serialize(QDataStream* dataStream) {
	if (dataStream == NULL)
		return false;
	for (int i = 1; i < this->metaObject()->propertyCount(); i++) {
		QMetaProperty prop = this->metaObject()->property(i);
		const char* propName = prop.name();
		*dataStream << (this->property(propName));
	}
	return true;
}

bool SerializedBase::unserialize(QDataStream* dataStream) {
	if (dataStream == NULL)
		return false;
	for (int i = 1; i < this->metaObject()->propertyCount(); i++) {
		QMetaProperty prop = this->metaObject()->property(i);
		const char* propName = prop.name();
		QVariant v;
		*dataStream >> v;
		this->setProperty(propName, v);
	}
	return true;
}

} /* namespace VBerry */
