/*
 * RequestUserInfo.cpp
 *
 *  Created on: 30 нояб. 2014 г.
 *      Author: sergey
 */

#include <VBerry/RequestUserInfo.hpp>
#include <VBerry/ResponseFriend.hpp>
#include <VBerry/AttachDownloader.hpp>
#include <QFileInfo>
#include <QDir>

namespace VBerry {

const QString RequestUserInfo::CACHE_FOLDER = "data/cache/";

RequestUserInfo::RequestUserInfo(QObject * parent)
		: QObject(parent) {
	manager = new OAuthConnection();
	contact = new ResponseFriend();
}

void RequestUserInfo::getByUserId(uint userId) {
	QMap<QString, QString> params;
	params.insert("user_ids", QString::number(userId));
	params.insert("fields", "screen_name,photo_200");
	qDebug() << params;
	currentReply = manager->requestApi("users.get", params);
	connect(currentReply, SIGNAL(finished()), this, SLOT(onReplyFinished()));
}

void RequestUserInfo::onReplyFinished() {
	QByteArray data = currentReply->readAll();
	qDebug() << data;
	bb::data::JsonDataAccess jda;
	QVariant friendsData = jda.loadFromBuffer(data);
	currentReply->deleteLater();
	if (jda.hasError()) {
		const bb::data::DataAccessError err = jda.error();
		const QString errorMsg = QString("Error converting JSON data: %1").arg(err.errorMessage());
		qDebug() << errorMsg;
		return;
	}

	QVariantList list = friendsData.value<QVariantMap>().value("response").value<QVariantList>();
	if (!list.length() && friendsData.value<QVariantMap>().value("error").value<QVariantMap>().contains("error_code")) {
		int error_code = friendsData.value<QVariantMap>().value("error").value<QVariantMap>().value("error_code").toInt();
		emit errorResponse(error_code);
		return;
	}

	contact->setId(list.at(0).toMap().value("id").toUInt());
	contact->setFirstName(list.at(0).toMap().value("first_name").toString());
	contact->setLastName(list.at(0).toMap().value("last_name").toString());
	contact->setScreenName(list.at(0).toMap().value("screen_name").toString());
	downloadUserPic(list.at(0).toMap().value("photo_200").toString());
}

void RequestUserInfo::onAttachAllDone() {
	emit userInfoResponse(contact);
}

void RequestUserInfo::downloadUserPic(const QString& url) {
	QString filename = QFileInfo(QUrl(url).path()).fileName();
	QString destPath = QFileInfo(RequestUserInfo::CACHE_FOLDER + filename).absoluteFilePath();

	AttachDownloader *downloader = new AttachDownloader();

	if (!QFile(destPath).exists()) {
		if (!QDir().exists(RequestUserInfo::CACHE_FOLDER)) {
			QDir dir;
			dir.mkpath(RequestUserInfo::CACHE_FOLDER);
		}
		downloader->download(url, destPath);
	}

	downloader->deleteLater();
	contact->setPhoto200(destPath);
	emit userInfoResponse(contact);
}

} /* namespace VBerry */

