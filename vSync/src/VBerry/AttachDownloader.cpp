/*
 * AttachDownloader.cpp
 *
 *  Created on: 29 нояб. 2014 г.
 *      Author: sergey
 */

#include <VBerry/AttachDownloader.hpp>
#include <bb/FileSystemInfo>
#include <global.hpp>

#ifdef HEADLESSAPP
#include <service.hpp>
#endif
#ifdef UIAPP
#include <applicationui.hpp>
#endif

namespace VBerry {

AttachDownloader::AttachDownloader(QObject * parent)
			: QObject(parent) {
	manager = new OAuthConnection();
	canDownload = true;
	currentStatus = AttachDownloader::Waiting;
	currentDownload = NULL;
	output = new QFile();
}

AttachDownloader::~AttachDownloader() {
	delete output;
	output = NULL;
	delete manager;
	manager = NULL;
}

void AttachDownloader::onReadyRead() {
	output->write(currentDownload->readAll());
}

void AttachDownloader::onFinished() {
	output->flush();
	output->close();
	currentDownload->deleteLater();
	qDebug() << "Download finished" << output->fileName();

	emit downloadFinished(output->fileName());
	startNextDownload();
}

void AttachDownloader::startNextDownload() {
	if (!canDownload) return;

	if (downloadQueue.length() == 0) {
		currentStatus = AttachDownloader::Waiting;
		emit allDone();
		return;
	}

	currentStatus = AttachDownloader::InProgress;

	QPair<QString, QString> pair = downloadQueue.dequeue();

	output->setFileName(pair.second);
	if (!output->open(QIODevice::WriteOnly)) {
		emit downloadError(output->errorString());
		startNextDownload();
		return;
	}

	currentDownload = manager->get(QNetworkRequest(QUrl( pair.first )));
	currentDownload->setReadBufferSize(2048);
	bool ok = connect(currentDownload, SIGNAL(metaDataChanged()), this, SLOT(onMetaDataChanged()));
	Q_ASSERT(ok);
	ok = connect(currentDownload, SIGNAL(readyRead()), this, SLOT(onReadyRead()));
	Q_ASSERT(ok);
	ok = connect(currentDownload, SIGNAL(finished()), this, SLOT(onFinished()));
	Q_ASSERT(ok);
	ok = connect(currentDownload, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(onError(QNetworkReply::NetworkError)));
	Q_ASSERT(ok);
}

void AttachDownloader::onMetaDataChanged() {
	qint64 dataSize = currentDownload->header(QNetworkRequest::ContentLengthHeader).toULongLong();
	QString directory = QFileInfo(output->fileName()).absolutePath();
	if (!deviceHasSufficientFileSpace(dataSize, directory)) {
		loop.exit(1);
		if (currentDownload != NULL) {
			currentDownload->abort();
			currentDownload->close();
			currentDownload->deleteLater();
			output->close();
			output->remove();
		}
		emit downloadError("Not enough space available");
	}
}

bool AttachDownloader::download(const QString &sourceFile, const QString &destFile) {
	if (!canDownload) return false;

	currentStatus = AttachDownloader::InProgress;

	output->setFileName(destFile);
	if (!output->open(QIODevice::WriteOnly)) {
		emit downloadError(output->errorString());
		return false;
	}

	currentDownload = manager->get(QNetworkRequest(QUrl( sourceFile )));
	currentDownload->setReadBufferSize(800);

	bool ok = connect(currentDownload, SIGNAL(metaDataChanged()), this, SLOT(onMetaDataChanged()));
	Q_ASSERT(ok);
	ok = connect(currentDownload, SIGNAL(readyRead()), this, SLOT(onReadyRead()));
	Q_ASSERT(ok);
	ok = connect(currentDownload, SIGNAL(finished()), this, SLOT(onFinished()));
	Q_ASSERT(ok);
	ok = connect(currentDownload, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(onError(QNetworkReply::NetworkError)));
	Q_ASSERT(ok);
	ok = connect(this, SIGNAL(allDone()), &loop, SLOT(quit()));
	Q_ASSERT(ok);

	qDebug() << "Loop started" << sourceFile << destFile;
	return !loop.exec();
}

void AttachDownloader::appendToDownload(const QString& sourceFile,
		const QString& destFile) {
	QPair<QString, QString> pair(sourceFile, destFile);
	downloadQueue.enqueue(pair);
}

void AttachDownloader::download() {
	startNextDownload();
}

void AttachDownloader::stopDownload() {
	if (currentDownload) {
		currentDownload->abort();
		currentDownload->close();
		currentDownload->deleteLater();
		output->close();
		output->remove();
	}
	canDownload = false;
	currentStatus = AttachDownloader::Paused;
}

void AttachDownloader::continueDownload() {
	canDownload = true;
	startNextDownload();
}

void AttachDownloader::onError(QNetworkReply::NetworkError code) {
	if (code) {
#ifdef HEADLESSAPP
		if (!Service::instance()->isConnected()) {
			stopDownload();
		}
#endif
#ifdef UIAPP
		if (!ApplicationUI::instance()->isConnected()) {
			stopDownload();
		}
#endif
	}

	output->close();
	output->remove();

	if (currentDownload) {
		currentDownload->deleteLater();
	}

	emit downloadError("Network connection problem: " + QString::number(code));
	loop.exit(1);
}

bool AttachDownloader::deviceHasSufficientFileSpace(qint64 dataSize, QString destinationFileSystem) {
	bb::FileSystemInfo fileSystemInfo;

	qint64 freeSpace = fileSystemInfo.availableFileSystemSpace(destinationFileSystem);
	if (freeSpace == -1) {
		qWarning() << "Failed to get free space - " << fileSystemInfo.errorString()
				   << "(" << fileSystemInfo.error() << ")";
		return(false);
	}
	return(freeSpace > dataSize);
}

AttachDownloader::Status AttachDownloader::status() {
	return currentStatus;
}

} /* namespace VBerry */
