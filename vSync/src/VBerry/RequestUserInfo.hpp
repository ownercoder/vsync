/*
 * RequestUserInfo.hpp
 *
 *  Created on: 30 нояб. 2014 г.
 *      Author: sergey
 */

#ifndef REQUESTUSERINFO_HPP_
#define REQUESTUSERINFO_HPP_

#include <QObject>
#include <VBerry/OAuthConnection.h>
#include <VBerry/ResponseFriend.hpp>
#include <bb/data/JsonDataAccess>

namespace VBerry {

class RequestUserInfo: public QObject {
	Q_OBJECT
public:
	explicit RequestUserInfo(QObject * parent = NULL);
	void getByUserId(uint userId);

	static const QString CACHE_FOLDER;
signals:
	void userInfoResponse(ResponseFriend * user);
	void errorResponse(int code);
protected slots:
	void onReplyFinished();
	void onAttachAllDone();
protected:
	void downloadUserPic(const QString &url);
	ResponseFriend *contact;
	OAuthConnection * manager;
	QNetworkReply *currentReply;
};

} /* namespace VBerry */

#endif /* REQUESTUSERINFO_HPP_ */
