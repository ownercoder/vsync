/*
 * RequestAudio.hpp
 *
 *  Created on: 30 нояб. 2014 г.
 *      Author: sergey
 */

#ifndef REQUESTAUDIO_HPP_
#define REQUESTAUDIO_HPP_

#include <QObject>
#include <VBerry/OAuthConnection.h>
#include <VBerry/ResponseAudio.hpp>
#include <bb/data/JsonDataAccess>

namespace VBerry {

class RequestAudio: public QObject {
	Q_OBJECT
public:
	explicit RequestAudio(QObject * parent = NULL);
	void getByAlbumId(uint albumId);
signals:
	void audioResponse(const QList<ResponseAudio*> & response);
	void errorResponse(int code);
protected Q_SLOTS:
	void onReplyFinished();
protected:
	OAuthConnection * manager;
	QNetworkReply *currentReply;
};

} /* namespace VBerry */

#endif /* REQUESTAUDIO_HPP_ */
