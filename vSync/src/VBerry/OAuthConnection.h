/*
 * OAuthConnection.h
 *
 *  Created on: 15 нояб. 2014 г.
 *      Author: sergey
 */

#ifndef OAUTHCONNECTION_H_
#define OAUTHCONNECTION_H_

#include <VBerry/Connection.h>
#include <QObject>
#include <QMap>
#include <QUrl>
#include <global.hpp>

#ifdef UIAPP
#include <bb/cascades/Page>
using namespace bb::cascades;
#endif

namespace VBerry {

class OAuthConnection: public Connection {
	Q_OBJECT
	Q_FLAGS(Scopes)
public:
	explicit OAuthConnection(QObject * parent = NULL);
	void actionRequired();
#ifdef UIAPP
	void signUpUser();
#endif
	QNetworkReply *requestApi(const QString &apiName, QMap<QString, QString> params);

	enum Scope {
		Notify        = 0x1,
		Friends       = 0x2,
		Photos        = 0x4,
		Audio         = 0x8,
		Video         = 0x10,
		Docs          = 0x20,
		Notes         = 0x40,
		Pages         = 0x80,
		Status        = 0x100,
		Offers        = 0x200,
		Questions     = 0x400,
		Wall          = 0x800,
		Groups        = 0x1000,
		Messages      = 0x2000,
		Notifications = 0x4000,
		Stats         = 0x8000,
		Ads           = 0x10000,
		Offline       = 0x20000
	};

	Q_DECLARE_FLAGS(Scopes, Scope)

	void setScopes(Scopes scopes);

signals:
	void errorDetected(const QString &error, const QString &errorDescription);
	void authSuccess();
#ifdef UIAPP
	void userActionRequired(bb::cascades::Page * page);
#endif
protected Q_SLOTS:
	void onUrlChanged(const QUrl &url);
protected:
	QUrl oauthUrl();
	static const QString AUTH_BASE;
	static const QString SIGNUP_BASE;
	static const QString BLANK_CALLBACK;
	static const char* scopeNames[];
private:
	QString token;
	uint user_id;
	Scopes _scopes;
};

Q_DECLARE_OPERATORS_FOR_FLAGS(OAuthConnection::Scopes)

} /* namespace Vberry */

#endif /* OAUTHCONNECTION_H_ */
