/*
 * ResponseAlbums.hpp
 *
 *  Created on: 24 нояб. 2014 г.
 *      Author: sergey
 */

#ifndef RESPONSEALBUMS_HPP_
#define RESPONSEALBUMS_HPP_

#include <VBerry/SerializedBase.hpp>

namespace VBerry {

class ResponseAlbums: public SerializedBase {
	Q_OBJECT
	Q_PROPERTY(QString title READ title WRITE setTitle)
	Q_PROPERTY(uint ownerId READ ownerId WRITE setOwnerId)
	Q_PROPERTY(uint albumId READ albumId WRITE setAlbumId)
public:
	ResponseAlbums();
	QString title();
	uint ownerId();
	uint albumId();
public slots:
	void setTitle(const QString &value);
	void setOwnerId(const uint &value);
	void setAlbumId(const uint &value);
protected:
	QString _title;
	uint _albumId;
	uint _ownerId;
};

} /* namespace VBerry */

#endif /* RESPONSEALBUMS_HPP_ */
