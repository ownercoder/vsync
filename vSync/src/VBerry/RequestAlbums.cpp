/*
 * RequestAlbums.cpp
 *
 *  Created on: 24 нояб. 2014 г.
 *      Author: sergey
 */

#include <VBerry/RequestAlbums.hpp>
#include <VBerry/ResponseAlbums.hpp>
#include <QNetworkReply>

namespace VBerry {

RequestAlbums::RequestAlbums(QObject *parent)
		: QObject(parent) {
	connection = new OAuthConnection();
}

void RequestAlbums::getAlbums() {
	QMap<QString, QString> params;
	params.insert("offset", "0");
	params.insert("count", "100");
	currentReply = connection->requestApi("audio.getAlbums", params);
	connect(currentReply, SIGNAL(finished()), this, SLOT(onReplyFinished()));
}

void RequestAlbums::onReplyFinished() {
	bb::data::JsonDataAccess jda;
	QVariant albumsData = jda.loadFromBuffer(currentReply->readAll());
	currentReply->deleteLater();
	if (jda.hasError()) {
		const bb::data::DataAccessError err = jda.error();
		const QString errorMsg = QString("Error converting JSON data: %1").arg(err.errorMessage());
		qDebug() << errorMsg;
		return;
	}

	QList<ResponseAlbums*> albums;

	QVariantList list = albumsData.value<QVariantMap>().value("response").value<QVariantList>();
	if (!list.length() && albumsData.value<QVariantMap>().value("error").value<QVariantMap>().contains("error_code")) {
		int error_code = albumsData.value<QVariantMap>().value("error").value<QVariantMap>().value("error_code").toInt();
		emit errorResponse(error_code);
		return;
	}
	for(int i = 1; i < list.length(); i++) {
		QString title = list.at(i).toMap().value("title").toString();
		uint ownerId = list.at(i).toMap().value("owner_id").toUInt();
		uint albumId = list.at(i).toMap().value("album_id").toUInt();

		ResponseAlbums * album = new ResponseAlbums();
		album->setTitle(title);
		album->setOwnerId(ownerId);
		album->setAlbumId(albumId);
		albums << album;
	}

	emit albumsResponse(albums);
}

void RequestAlbums::cancel() {
	currentReply->abort();
	currentReply->deleteLater();
}

} /* namespace VBerry */
