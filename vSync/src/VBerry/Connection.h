/*
 * Connection.h
 *
 *  Created on: 15 нояб. 2014 г.
 *      Author: sergey
 */

#ifndef CONNECTION_H_
#define CONNECTION_H_

#include <QNetworkAccessManager>

namespace VBerry {

class Connection: public QNetworkAccessManager {
	Q_OBJECT
public:
	explicit Connection(QObject * parent = NULL);
	virtual ~Connection();
protected:
	static const QString APP_ID;
	static const QString SECRET_KEY;
	static const QString API_BASE;
	static const QString API_VERSION;
};

} /* namespace Vberry */

#endif /* CONNECTION_H_ */
