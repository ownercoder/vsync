/*
 * ResponseAudio.hpp
 *
 *  Created on: 30 нояб. 2014 г.
 *      Author: sergey
 */

#ifndef RESPONSEAUDIO_HPP_
#define RESPONSEAUDIO_HPP_

#include <VBerry/SerializedBase.hpp>

namespace VBerry {

class ResponseAudio: public SerializedBase {
	Q_OBJECT
	Q_PROPERTY(uint id READ getId WRITE setId)
	Q_PROPERTY(QString artist READ getArtist WRITE setArtist)
	Q_PROPERTY(QString title READ getTitle WRITE setTitle)
	Q_PROPERTY(QString url READ getUrl WRITE setUrl)
public:
	ResponseAudio();

	const QString& getArtist() const {
		return _artist;
	}

	void setArtist(const QString& artist) {
		_artist = artist;
	}

	uint getId() const {
		return _id;
	}

	void setId(uint id) {
		_id = id;
	}

	const QString& getTitle() const {
		return _title;
	}

	void setTitle(const QString& title) {
		_title = title;
	}

	const QString& getUrl() const {
		return _url;
	}

	void setUrl(const QString& url) {
		_url = url;
	}

protected:
	uint _id;
	QString _artist;
	QString _title;
	QString _url;
};

} /* namespace VBerry */

#endif /* RESPONSEAUDIO_HPP_ */
