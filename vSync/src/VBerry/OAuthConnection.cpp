/*
 * OAuthConnection.cpp
 *
 *  Created on: 15 нояб. 2014 г.
 *      Author: sergey
 */
#include <VBerry/utils.h>
#include <VBerry/OAuthConnection.h>
#include <src/Settings.h>
#include <QNetworkRequest>

#include <global.hpp>

#ifdef UIAPP
#include <bb/cascades/Application>
#include <bb/cascades/QmlDocument>
#include <bb/cascades/WebView>
#include <bb/cascades/WebStorage>
#endif


namespace VBerry {

const QString OAuthConnection::AUTH_BASE = "https://oauth.vk.com/authorize";
const QString OAuthConnection::SIGNUP_BASE = "https://oauth.vk.com/join";
const QString OAuthConnection::BLANK_CALLBACK = "https://oauth.vk.com/blank.html";

const char * OAuthConnection::scopeNames[] = { "notify", "friends", "photos", "audio",
	"video", "docs", "notes", "pages", "status", "offers", "questions", "wall",
	"groups", "messages", "notifications", "stats", "ads", "offline" };

OAuthConnection::OAuthConnection(QObject * parent)
		: Connection(parent) {
	token = Settings::instance()->token();
	user_id = Settings::instance()->userId();
	qDebug() << "Create OAuthConnection" << token << user_id;
}

#ifdef UIAPP
void OAuthConnection::actionRequired() {
	bb::cascades::Page * parentPage = new bb::cascades::Page();

	QmlDocument *qml = QmlDocument::create("asset:///vklogin.qml");

	Control* control = qml->createRootObject<Control>();

	WebView *loginWebView = control->findChild<WebView*>("loginWebView");
	loginWebView->storage()->clearCookies();

	connect(loginWebView, SIGNAL(urlChanged(const QUrl&)), this, SLOT(onUrlChanged(const QUrl&)));
	loginWebView->setUrl(oauthUrl());
	loginWebView->reload();

	parentPage->setContent(control);

	emit userActionRequired(parentPage);
}
#endif

QUrl OAuthConnection::oauthUrl() {
	QUrl url(AUTH_BASE);
	url.addQueryItem("client_id", APP_ID);
	url.addQueryItem("scope", flagsToStrList(_scopes, scopeNames).join(","));
	url.addQueryItem("redirect_uri", BLANK_CALLBACK);
	url.addQueryItem("display", "mobile");
	url.addQueryItem("v", API_VERSION);
	url.addQueryItem("response_type", "token");
	qDebug() << url;
	return url;
}

void OAuthConnection::setScopes(Scopes scopes) {
	_scopes = scopes;
}

void OAuthConnection::onUrlChanged(const QUrl &url) {
	if (url.hasFragment()) {
		QUrl convertedUrl(url.toString().replace("#", "?")); // hack for fragment
		QString error = convertedUrl.queryItemValue("error");
		QString errorDescription = convertedUrl.queryItemValue("error_description");
		QString token = convertedUrl.queryItemValue("access_token");
		uint userId = convertedUrl.queryItemValue("user_id").toUInt();
		if (error.count() == 0 && errorDescription.count() == 0) {
			Settings::instance()->setToken(token);
			Settings::instance()->setUserId(userId);
			emit authSuccess();
		} else {
			emit errorDetected(error, errorDescription);
		}
	}
}

QNetworkReply *OAuthConnection::requestApi(const QString& apiName, QMap<QString, QString> params) {
	QUrl url("https://api.vk.com/");
	url.setPath("method/" + apiName);
	for(int i = 0;i < params.keys().length();i++) {
		QString key = params.keys().at(i);
		url.addQueryItem(key, params.value(key));
	}
	url.addQueryItem("access_token", Settings::instance()->token());
	qDebug() << url.toString();
	return this->get(QNetworkRequest(url));
}

#ifdef UIAPP
void OAuthConnection::signUpUser() {
	bb::cascades::Page * parentPage = new bb::cascades::Page();

	QmlDocument *qml = QmlDocument::create("asset:///vklogin.qml");

	Control* control = qml->createRootObject<Control>();

	QUrl url(SIGNUP_BASE);
	url.addQueryItem("client_id", APP_ID);
	url.addQueryItem("scope", flagsToStrList(_scopes, scopeNames).join(","));
	url.addQueryItem("redirect_uri", BLANK_CALLBACK);
	url.addQueryItem("display", "mobile");
	url.addQueryItem("v", API_VERSION);
	url.addQueryItem("response_type", "token");
	qDebug() << url;

	WebView *loginWebView = control->findChild<WebView*>("loginWebView");
	loginWebView->storage()->clearCookies();
	connect(loginWebView, SIGNAL(urlChanged(const QUrl&)), this, SLOT(onUrlChanged(const QUrl&)));
	loginWebView->setUrl(url);
	loginWebView->reload();

	parentPage->setContent(control);

	emit userActionRequired(parentPage);
}
#endif

} /* namespace Vberry */

