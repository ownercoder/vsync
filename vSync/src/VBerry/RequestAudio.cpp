/*
 * RequestAudio.cpp
 *
 *  Created on: 30 нояб. 2014 г.
 *      Author: sergey
 */

#include <VBerry/RequestAudio.hpp>
#include <VBerry/ResponseAudio.hpp>
#include <QNetworkReply>

namespace VBerry {

RequestAudio::RequestAudio(QObject * parent)
		: QObject(parent) {
	manager = new OAuthConnection();
}

void RequestAudio::getByAlbumId(uint albumId) {
	QMap<QString, QString> params;
	params.insert("album_id", QString::number(albumId));
	params.insert("offset", "0");
	params.insert("count", "1000");
	currentReply = manager->requestApi("audio.get", params);
	connect(currentReply, SIGNAL(finished()), this, SLOT(onReplyFinished()));
}

void RequestAudio::onReplyFinished() {
	bb::data::JsonDataAccess jda;
	QByteArray data = currentReply->readAll();
//	qDebug() << data;
	QVariant audioData = jda.loadFromBuffer(data);
	currentReply->deleteLater();
	if (jda.hasError()) {
		const bb::data::DataAccessError err = jda.error();
		const QString errorMsg = QString("Error converting JSON data: %1").arg(err.errorMessage());
		qDebug() << errorMsg;
		return;
	}

	QList<ResponseAudio*> audios;

	QVariantList list = audioData.value<QVariantMap>().value("response").value<QVariantList>();
	if (!list.length() && audioData.value<QVariantMap>().value("error").value<QVariantMap>().contains("error_code")) {
		int error_code = audioData.value<QVariantMap>().value("error").value<QVariantMap>().value("error_code").toInt();
		emit errorResponse(error_code);
		return;
	}
	for(int i = 0; i < list.length(); i++) {
		ResponseAudio * audio = new ResponseAudio();
		audio->setId( list.at(i).toMap().value("id").toUInt() );
		audio->setArtist( list.at(i).toMap().value("artist").toString() );
		audio->setTitle( list.at(i).toMap().value("title").toString() );
		audio->setUrl( list.at(i).toMap().value("url").toString() );

		audios << audio;
	}

	emit audioResponse(audios);
}

} /* namespace VBerry */
