/*
 * Settings.cpp
 *
 *  Created on: 10 нояб. 2014 г.
 *      Author: sergey
 */

#include <src/Settings.h>
#include <QDebug>
#include <QSettings>

Settings * Settings::_instance = NULL;

Settings::Settings() {
	QSettings setting;
	if (!setting.contains("token")) {
		setToken("");
	}
	setting.sync();
}

Settings* Settings::instance() {
	if (!_instance)
		_instance = new Settings();
	return _instance;
}

QString Settings::fileName() {
	return setting.fileName();
}
void Settings::setToken(const QString& token) {
	setting.setValue("token", token);
}

void Settings::setUserId(const uint& user_id) {
	setting.setValue("user_id", user_id);
}

void Settings::setSyncContacts(const bool& ok) {
	setting.setValue("sync_contancts", ok);
}

void Settings::setSyncAudio(const bool& ok) {
	setting.setValue("sync_audio", ok);
}

void Settings::setSyncViaMobileData(const bool& ok) {
	setting.setValue("sync_via_mobile_network", ok);
}

QString Settings::token() {
	return setting.value("token").toString();
}

uint Settings::userId() {
	return setting.value("user_id").toUInt();
}

bool Settings::syncContacts() {
	return setting.value("sync_contancts", false).toBool();
}

bool Settings::syncAudio() {
	return setting.value("sync_audio", false).toBool();
}

bool Settings::syncViaMobileData() {
	return setting.value("sync_via_mobile_network", false).toBool();
}

void Settings::setAlbumId(const uint& album_Id) {
	setting.setValue("album_id", album_Id);
}

uint Settings::albumId() {
	return setting.value("album_id").toUInt();
}

void Settings::setAlbumsCache(const QByteArray& data) {
	setting.setValue("albums_cache", data);
}

QByteArray Settings::albumsCache() {
	return setting.value("albums_cache").toByteArray();
}

void Settings::setFriendsCache(const QByteArray& data) {
	setting.setValue("friends_cache", data);
}

QByteArray Settings::friendsCache() {
	return setting.value("friends_cache").toByteArray();
}

void Settings::setUserInfoCache(const QByteArray &data) {
	setting.setValue("user_info_cache", data);
}

QByteArray Settings::userInfoCache() {
	return setting.value("user_info_cache").toByteArray();
}

void Settings::setSyncActionState(Settings::SyncActionState state) {
	setting.setValue("sync_action_state", state);
}

Settings::SyncActionState Settings::syncActionState() {
	return static_cast<SyncActionState>(setting.value("sync_action_state").toInt());
}

void Settings::addSyncedContactId(uint user_id, uint contactId) {
	QHash<QString, QVariant> contactIds = setting.value("contacts_synced_hash").toHash();

	contactIds.insert(QString::number(user_id), contactId);

	setting.setValue("contacts_synced_hash", contactIds);
	setting.sync();
}

bool Settings::isContactSynced(uint user_id) {
	QHash<QString, QVariant> contactIds = setting.value("contacts_synced_hash").toHash();
	return contactIds.contains(QString::number(user_id));
}

void Settings::reload() {
	setting.sync();
}
