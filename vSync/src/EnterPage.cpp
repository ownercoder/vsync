/*
 * EnterPage.cpp
 *
 *  Created on: 23 нояб. 2014 г.
 *      Author: sergey
 */

#include <EnterPage.hpp>
#include <bb/cascades/Application>
#include <bb/cascades/QmlDocument>
#include <bb/cascades/Button>
#include <VBerry/OAuthConnection.h>
#include <applicationui.hpp>
#include <bb/cascades/NavigationPane>
#include <bb/system/SystemDialog>

using namespace bb::system;
using namespace bb::cascades;

EnterPage::EnterPage() {
	QmlDocument *qml = QmlDocument::create("asset:///enter.qml");

	Control* page = qml->createRootObject<Control>();

	Button *signinButton = page->findChild<Button*>("signinButton");
	Button *signupButton = page->findChild<Button*>("signupButton");

	connect(signinButton, SIGNAL(clicked()), this, SLOT(onSignInClicked()));
	connect(signupButton, SIGNAL(clicked()), this, SLOT(onSignUpClicked()));

	this->setContent(page);

	oauthConnection = new OAuthConnection();
	connect(oauthConnection, SIGNAL(userActionRequired(bb::cascades::Page *)), this, SLOT(onUserActionRequired(bb::cascades::Page *)));
	connect(oauthConnection, SIGNAL(authSuccess()), this, SLOT(onAuthSuccess()));
	connect(oauthConnection, SIGNAL(errorDetected(const QString &, const QString &)), this, SLOT(onErrorDetected(const QString &, const QString &)));

	oauthConnection->setScopes(OAuthConnection::Friends | OAuthConnection::Audio | OAuthConnection::Offline);

	Application::instance()->setMenuEnabled(false);
}

void EnterPage::onSignInClicked() {
	if (ApplicationUI::instance()->isConnected()) {
		oauthConnection->actionRequired();
	} else {
		ApplicationUI::instance()->showNoNetworkAvailableDialog();
	}
}

void EnterPage::onSignUpClicked() {
	if (ApplicationUI::instance()->isConnected()) {
		oauthConnection->signUpUser();
	} else {
		ApplicationUI::instance()->showNoNetworkAvailableDialog();
	}
}

void EnterPage::onUserActionRequired(bb::cascades::Page * page) {
	ApplicationUI::instance()->nav->setBackButtonsVisible(true);
	ApplicationUI::instance()->nav->push(page);
}

void EnterPage::onAuthSuccess() {
	emit authCompleated();
}

void EnterPage::onErrorDetected(const QString &error, const QString &errorDescription) {
	ApplicationUI::instance()->nav->pop()->deleteLater();

	SystemDialog *dialog = new SystemDialog("Try again",
											"Cancel");

	dialog->setTitle("Access denied");

	dialog->setBody("VK access denied, details: " + errorDescription);

	dialog->setEmoticonsEnabled(true);

	// Connect the finished() signal to the onDialogFinished() slot.
	// The slot will check the SystemUiResult to see which
	// button was tapped.
	bool success = connect(dialog,
		SIGNAL(finished(bb::system::SystemUiResult::Type)),
		this,
		SLOT(onAccessDeniedDialog(bb::system::SystemUiResult::Type)));

	if (success) {
		dialog->show();
	} else {
		dialog->deleteLater();
	}
	qDebug() << error << errorDescription;
}

void EnterPage::onAccessDeniedDialog(bb::system::SystemUiResult::Type type) {
	if (type == bb::system::SystemUiResult::ConfirmButtonSelection) {
		onSignInClicked();
	}
}
