/*
 * Contacts.cpp
 *
 *  Created on: 29 нояб. 2014 г.
 *      Author: sergey
 */

#include <Phone/Contacts.hpp>
#include <bb/pim/contacts/ContactPhotoBuilder>
#include <QFileInfo>
#include <QRegExp>
#include <Settings.h>
#include <QStringList>
#include <QFileInfo>
#include <QDir>

namespace Phone {

Contacts::Contacts(QObject * parent) :
		QObject(parent) {
	downloader = new AttachDownloader();
}

/*void Contacts::onDownloadFinished(const QString &file) {
	int contactId = hash[file];
	if (!contactId) {
		return;
	}

	Contact contact = ContactService().contactDetails(contactId);
	if (!contact.id()) {
		return;
	}

	qDebug() << "Downloaded file: " << file;

	ContactBuilder builder = contact.edit();
	builder.addPhoto(ContactPhotoBuilder()
					.setOriginalPhoto(file)
					.setPrimaryPhoto(true), true);
	ContactService().updateContact(builder);
}*/

/*void Contacts::onDownloadError(const QString &message) {
	qDebug() << "Downloade error: " << message;
}*/

void Contacts::processContact(ResponseFriend *friendContact) {
	QString mobilePhone = getPhoneNumbers(friendContact->getMobilePhone());
	QString homePhone = getPhoneNumbers(friendContact->getHomePhone());
	if (mobilePhone.length() >= 10) {
		create(friendContact);
	}
}

void Contacts::create(ResponseFriend *friendContact) {
	QString formatedMobilePhone = formatPhoneNumber(friendContact->getMobilePhone());
	QString formatedHomePhone = formatPhoneNumber(friendContact->getHomePhone());
	qDebug() << formatedMobilePhone << formatedHomePhone;

	if (formatedMobilePhone.length() == 0) return;

	ContactBuilder builder;
	qDebug() << friendContact->getPhoto().toString();

	builder.addPhoto(ContactPhotoBuilder()
				.setOriginalPhoto(friendContact->getPhoto().toString())
				.setPrimaryPhoto(true), true);

	builder.addAttribute(ContactAttributeBuilder()
							.setKind(AttributeKind::Name)
							.setSubKind(AttributeSubKind::NameGiven)
							.setValue(friendContact->getFirstName()));

	builder.addAttribute(ContactAttributeBuilder()
							.setKind(AttributeKind::Name)
							.setSubKind(AttributeSubKind::NameSurname)
							.setValue(friendContact->getLastName()));

	builder.addAttribute(ContactAttributeBuilder()
							.setKind(AttributeKind::Phone)
							.setSubKind(AttributeSubKind::PhoneMobile)
							.setValue(formatedMobilePhone));

	if (formatedHomePhone.length()) {
		builder.addAttribute(ContactAttributeBuilder()
								.setKind(AttributeKind::Phone)
								.setSubKind(AttributeSubKind::Other)
								.setValue(formatedHomePhone));
	}

	QString destFile = getDestFileName(friendContact->getPhoto());

	if (!QFile::exists(destFile)) {
		if (!downloader->download(friendContact->getPhoto().toString(), destFile)) {
			return;
		}
	}

	builder.addPhoto(ContactPhotoBuilder()
								.setOriginalPhoto(destFile)
								.setPrimaryPhoto(true), true);

	Contact contact = ContactService().createContact(builder, false);
	if (contact.id()) {
		Settings::instance()->addSyncedContactId(friendContact->getId(), contact.id());
	}
}

/*void Contacts::applyPhotos() {
	downloader->download();
}*/

/*void Contacts::onAllDone() {
	downloader->deleteLater();
	emit syncCompleated();
}*/

QString Contacts::getDestFileName(const QUrl &url) {
	QString directory = "shared/misc/contacts_photo";
	QDir dir;
	if (!dir.exists(directory)) {
		dir.mkpath(directory);
	}

	return QFileInfo(directory + "/" + url.path().replace("/", "_")).absoluteFilePath();
}

/*void Contacts::update(Contact *contact, ResponseFriend *friendContact) {
	QString formatedHomePhone = formatPhoneNumber(friendContact->getHomePhone());
	QString homePhone = getPhoneNumbers(friendContact->getHomePhone());
	qDebug() << "Contact id: " << contact->id();
	Contact editableContact = this->contactDetails(contact->id());

	ContactBuilder builder = editableContact.edit();
	QString absoluteFilePath = QFileInfo(friendContact->getPhoto().toString()).absoluteFilePath();
	builder.addPhoto(ContactPhotoBuilder()
			.setOriginalPhoto(absoluteFilePath)
			.setPrimaryPhoto(true));

	QList<ContactAttribute> phones = editableContact.phoneNumbers();

	if (homePhone.length()) {
		bool exist = false;
		for(int i=0;i<phones.length();i++) {
			if (phones.at(i).value().contains(homePhone)) {
				exist = true;
			}
		}

		if (!exist) {
			builder.addAttribute(ContactAttributeBuilder()
						.setKind(AttributeKind::Phone)
						.setSubKind(AttributeSubKind::Other)
						.setValue(formatedHomePhone));
		}
	}
	this->updateContact(builder);
}*/

QString Contacts::formatPhoneNumber(const QString &phoneNumber) {
	QString phone = getPhoneNumbers(phoneNumber);

	if (phone.length() == 10) {
		phone = "+7" + phone;
	} else if (phone.length() == 11 && phone.contains(QRegExp("^8"))) {
		phone = phone.replace(QRegExp("^8"), "+7");
	}

	return phone;
}

QString Contacts::getPhoneNumbers(const QString& phoneNumber) {
	QRegExp rx("([\\d\\+]+)");

	int pos = 0;
	QStringList list;

	while ((pos = rx.indexIn(phoneNumber, pos)) != -1) {
		list << rx.cap(1);
		pos += rx.matchedLength();
	}

	QString phone = "";

	if (list.length()) {
		phone = list.join("");
	}

	return phone.replace(QRegExp("(^8|^\\+7)"), "");
}

} /* namespace Phone */
