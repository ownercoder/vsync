/*
 * Contacts.hpp
 *
 *  Created on: 29 нояб. 2014 г.
 *      Author: sergey
 */

#ifndef CONTACTS_HPP_
#define CONTACTS_HPP_

#include <QObject>
#include <VBerry/ResponseFriend.hpp>
#include <bb/pim/contacts/ContactService>
#include <bb/pim/contacts/ContactBuilder>
#include <bb/pim/contacts/ContactPostalAddressBuilder>
#include <bb/pim/contacts/ContactAttributeBuilder>
#include <bb/pim/contacts/Contact>
#include <VBerry/AttachDownloader.hpp>

namespace Phone {

using namespace VBerry;
using namespace bb::pim::contacts;

class Contacts: public QObject {
	Q_OBJECT
public:
	explicit Contacts(QObject * parent = 0);
	virtual ~Contacts() {};
	void processContact(ResponseFriend *friendContact);
	//void applyPhotos();
//signals:
	//void syncCompleated();
/*
protected Q_SLOTS:
	void onDownloadFinished(const QString &file);
	void onDownloadError(const QString &message);
	void onAllDone();
*/
protected:
	//void update(Contact *contact, ResponseFriend *friendContact);
	void create(ResponseFriend *friendContact);
	QString getPhoneNumbers(const QString &phoneNumber);
	QString formatPhoneNumber(const QString &phoneNumber);
	QString getDestFileName(const QUrl &url);
private:
	AttachDownloader *downloader;
};

} /* namespace Phone */

#endif /* CONTACTS_HPP_ */
