/*
 * Settings.h
 *
 *  Created on: 10 нояб. 2014 г.
 *      Author: sergey
 */

#ifndef SETTINGS_H_
#define SETTINGS_H_

#include <QObject>
#include <QSettings>

class Settings : public QObject {
	Q_OBJECT
public:
	static Settings * instance();
	void reload();
	enum SyncActionState {
		SYNC_PAUSE,
		SYNC_CONTINUE
	};
	Q_INVOKABLE void setToken(const QString &token);
	Q_INVOKABLE void setUserId(const uint &user_id);
	Q_INVOKABLE void setSyncContacts(const bool &ok);
	Q_INVOKABLE void setSyncAudio(const bool &ok);
	Q_INVOKABLE void setSyncViaMobileData(const bool &ok);
	Q_INVOKABLE void setAlbumId(const uint &albumId);
	Q_INVOKABLE void setAlbumsCache(const QByteArray &data);
	Q_INVOKABLE void setFriendsCache(const QByteArray &data);
	Q_INVOKABLE void setUserInfoCache(const QByteArray &data);
	Q_INVOKABLE void setSyncActionState(Settings::SyncActionState state);
	Q_INVOKABLE void addSyncedContactId(uint user_id, uint contactId);

	Q_INVOKABLE bool isContactSynced(uint user_id);
	Q_INVOKABLE QString token();
	Q_INVOKABLE QString fileName();
	Q_INVOKABLE uint userId();
	Q_INVOKABLE bool syncContacts();
	Q_INVOKABLE bool syncAudio();
	Q_INVOKABLE bool syncViaMobileData();
	Q_INVOKABLE uint albumId();
	Q_INVOKABLE QByteArray albumsCache();
	Q_INVOKABLE QByteArray friendsCache();
	Q_INVOKABLE QByteArray userInfoCache();
	Q_INVOKABLE Settings::SyncActionState syncActionState();
protected:
	Settings();
	virtual ~Settings() { };
private:
	static Settings * _instance;
	QSettings setting;
};

#endif /* SETTINGS_H_ */
