/*
 * MainWindow.cpp
 *
 *  Created on: 08 нояб. 2014 г.
 *      Author: sergey
 */

#include <MainWindow.h>
#include <applicationui.hpp>
#include <Settings.h>
#include <VBerry/ResponseAlbums.hpp>
#include <VBerry/RequestUserInfo.hpp>

#include <bb/cascades/Application>
#include <bb/cascades/QmlDocument>
#include <bb/cascades/AbstractPane>
#include <bb/cascades/Page>
#include <bb/cascades/Button>
#include <bb/cascades/TitleBar>
#include <bb/cascades/FreeFormTitleBarKindProperties>
#include <bb/cascades/ImageView>
#include <bb/cascades/Label>
#include <bb/cascades/SettingsActionItem>
#include <bb/system/SystemToast>
#include <bb/cascades/Menu>

#include <tc/utils/ConnectionInfo.hpp>

using namespace tc::utils;
using namespace bb::system;

MainWindow::MainWindow() {
	currentState = Settings::instance()->syncActionState();

	createApplicationMenu();

	requestAlbums = new RequestAlbums();
	connect(requestAlbums, SIGNAL(errorResponse(int)), this, SLOT(onResponseError(int)));
	connect(requestAlbums, SIGNAL(albumsResponse(const QList<ResponseAlbums*>&)), this, SLOT(onAlbumsReceived(const QList<ResponseAlbums*>&)));

	QmlDocument *qml = QmlDocument::create("asset:///main.qml");

	qml->setContextProperty("_settings", Settings::instance());
	qml->setContextProperty("_requestAlbums", requestAlbums);
	qml->setContextProperty("_app", this);

	Control* page = qml->createRootObject<Control>();

	dropDownAlbum 		= page->findChild<DropDown*>("dropDownAlbum");
	progressIndicator 	= page->findChild<ProgressIndicator*>("progressIndicator");
	statusLabel 		= page->findChild<Label*>("statusLabel");

	fillAblums();

	createPageMenu();

	this->setContent(page);

	pageTitleBar = new TitleBar(TitleBarKind::FreeForm);
	pageTitleBar->setAppearance(bb::cascades::TitleBarAppearance::Plain);
	this->setTitleBar(pageTitleBar);

	QTimer::singleShot(2000, this, SLOT(getUserInfo()));

	m_invokeManager = new InvokeManager();

	progress = new Progress();

	QTimer *timer = new QTimer();
	connect(timer, SIGNAL(timeout()), this, SLOT(updateProgress()));
	timer->setInterval(1000);
	timer->start();
	//connect(progress, SIGNAL(progressChanged()), this, SLOT(updateProgress()));

	updateProgress();

	this->setResizeBehavior(PageResizeBehavior::Resize);
}

void MainWindow::createApplicationMenu() {
	Application::instance()->setMenuEnabled(true);
	Menu *menu = new Menu();

	SettingsActionItem *exitItem = SettingsActionItem::create().image(Image("asset:///images/ic_exit.png"));
	connect(exitItem, SIGNAL(triggered()), this, SLOT(onExitTriggered()));

	menu->setSettingsAction(exitItem);
	exitItem->setTitle(tr("Exit"));
	//menu->addAction(exitItem);

	Application::instance()->setMenu(menu);
}

void MainWindow::setValue(const QString& key, const QString& value) {
	QSettings settings(ApplicationUI::APP_AUTHOR, ApplicationUI::APP_NAME);
	qDebug() << "Set key: " << key << " value: " << value;
	settings.setValue(key, value);
}

QString MainWindow::getValue(const QString& key) {
	QSettings settings(ApplicationUI::APP_AUTHOR, ApplicationUI::APP_NAME);
	return settings.value(key).toString();
}

void MainWindow::showProgressToast(const QString &message, const QString &cancelTitle) {
	progressToast = new SystemProgressToast(this);
	SystemUiButton *toastCancelButton = progressToast->button();
	connect(toastCancelButton, SIGNAL(enabledChanged(bool)), this, SLOT(onToastEnabledChanged(bool)));
	toastCancelButton->setLabel(cancelTitle);

	progressToast->setBody(message);
	progressToast->setState(SystemUiProgressState::Active);
	progressToast->setPosition(SystemUiPosition::MiddleCenter);
	progressToast->show();
}

void MainWindow::onAlbumsReceived(const QList<ResponseAlbums*> & albums) {
	QByteArray albumsCache;
	QDataStream out(&albumsCache, QIODevice::WriteOnly);

	dropDownAlbum->removeAll();

	for(int i = 0; i < albums.length(); i++) {
		albums.at(i)->serialize(&out);

		dropDownAlbum->add(Option::create().text(albums.at(i)->title())
				.value(albums.at(i)->albumId())
				.selected(Settings::instance()->albumId() == albums.at(i)->albumId()));
		qDebug() << (Settings::instance()->albumId() == albums.at(i)->albumId()) << " " << Settings::instance()->albumId();
	}

	Settings::instance()->setAlbumsCache(albumsCache);
	progressToast->cancel();
}

void MainWindow::fillAblums() {
	QByteArray albumsCache = Settings::instance()->albumsCache();
	QDataStream in(&albumsCache, QIODevice::ReadOnly);

	dropDownAlbum->removeAll();

	while(!in.atEnd()) {
		ResponseAlbums * album = new ResponseAlbums();
		if (album->unserialize(&in)) {
			dropDownAlbum->add(Option::create().text(album->title())
						.value(album->albumId())
						.selected(Settings::instance()->albumId() == album->albumId()));
		}
	}
}

void MainWindow::onResponseError(int code) {
	qDebug() << "API response error code:" << code;
	onExitTriggered();
}

void MainWindow::onToastEnabledChanged(bool changed) {
	qDebug() << changed;
	requestAlbums->cancel();
}

void MainWindow::onInterfaceNameChanged(QString interface) {
	qDebug() << interface;
}

void MainWindow::onUserInfoResponse(ResponseFriend* user) {
	setTitle(user);
	user->deleteLater();
}

void MainWindow::setTitle(ResponseFriend* user) {
	FreeFormTitleBarKindProperties *kindProps = new FreeFormTitleBarKindProperties();

	QmlDocument *qml = QmlDocument::create("asset:///header.qml");
	Control* page = qml->createRootObject<Control>();

	ImageView *userPic = page->findChild<ImageView*>("userPic");
	qDebug() << user->getPhoto200().toString() << "User pic";
	userPic->setImage(Image(user->getPhoto200().toString()));

	Label *userName = page->findChild<Label*>("userName");
	userName->setText(user->getLastName() + " " + user->getFirstName());

	Label *userId = page->findChild<Label*>("userId");
	userId->setText(user->getScreenName());

	kindProps->setContent(page);

	qDebug() << "Set title";
	this->titleBar()->setKindProperties(kindProps);
}

void MainWindow::getUserInfo() {
	if (!ApplicationUI::instance()->isConnected()) {
		SystemToast * toast = new SystemToast;
		toast->setBody("No internet connection available.");
		toast->setPosition(SystemUiPosition::BottomCenter);
		toast->show();

		QTimer::singleShot(10000, this, SLOT(getUserInfo()));
	} else {
		RequestUserInfo *userInfoRequest = new RequestUserInfo();
		connect(userInfoRequest, SIGNAL(errorResponse(int)), this, SLOT(onResponseError(int)));
		connect(userInfoRequest, SIGNAL(userInfoResponse(ResponseFriend*)), this, SLOT(onUserInfoResponse(ResponseFriend*)));
		userInfoRequest->getByUserId(Settings::instance()->userId());
	}
}

void MainWindow::onExitTriggered() {
	Settings::instance()->setToken("");
	Settings::instance()->setUserId(0);
	qDebug() << ApplicationUI::instance()->nav->count();
	ApplicationUI::instance()->nav->pop();
}

void MainWindow::createPageMenu() {
	itemPause = ActionItem::create().title(tr("Pause"));
	itemSync  = ActionItem::create().title(tr("Sync")).image(Image("asset:///images/ic_sync.png"));
	updatePauseAction();

	itemSync->setEnabled(false);

	connect(itemPause, SIGNAL(triggered()), this, SLOT(onActionPauseTriggered()));
	connect(itemSync, SIGNAL(triggered()), this, SLOT(onActionSyncTriggered()));

	addAction(itemSync, ActionBarPlacement::OnBar);
	addAction(itemPause, ActionBarPlacement::OnBar);
}

void MainWindow::onActionPauseTriggered() {
	bb::system::InvokeRequest request;
	if (currentState == Settings::SYNC_PAUSE) {
		request.setTarget("com.slytech.vSyncHeadlessService");
		request.setAction("com.slytech.vSyncHeadlessService.SYNC_PAUSE");
	} else if (currentState == Settings::SYNC_CONTINUE) {
		request.setTarget("com.slytech.vSyncHeadlessService");
		request.setAction("com.slytech.vSyncHeadlessService.SYNC_CONTINUE");
	}
	m_invokeManager->invoke(request);
	updatePauseAction();
}

void MainWindow::onActionSyncTriggered() {
	bb::system::InvokeRequest request;
	request.setTarget("com.slytech.vSyncHeadlessService");
	request.setAction("com.slytech.vSyncHeadlessService.SYNC_FORCE");
	m_invokeManager->invoke(request);
}

void MainWindow::updatePauseAction() {
	if (currentState == Settings::SYNC_CONTINUE) {
		currentState = Settings::SYNC_PAUSE;
		itemPause->setImage(Image("asset:///images/ic_pause.png"));
		itemPause->setTitle(tr("Pause"));
		Settings::instance()->setSyncActionState(Settings::SYNC_CONTINUE);
	} else {
		currentState = Settings::SYNC_CONTINUE;
		itemPause->setImage(Image("asset:///images/ic_resume.png"));
		itemPause->setTitle(tr("Continue"));
		Settings::instance()->setSyncActionState(Settings::SYNC_PAUSE);
	}
}

QString MainWindow::saveFileName(const QUrl &url) {
	QString path = url.path();
	QString basename = QFileInfo(path).fileName();

	QString basePath = "shared/misc/vksync/contact_photos/";

	if (basename.isEmpty())
		basename = "download";

	if (QFile::exists(basePath + basename)) {
		// already exists, don't overwrite
		int i = 0;
		basename += '.';
		while (QFile::exists(basePath + basename + QString::number(i)))
			++i;

		basename += QString::number(i);
	}

	return basePath + basename;
}

void MainWindow::onConnectionStateChanged(bool state) {
	qDebug() << "State" << state;
	if (state) {
		itemSync->setEnabled(true);
	} else {
		itemSync->setEnabled(false);
	}
}

void MainWindow::updateProgress() {
	qDebug() << "Update progress";
	progress->sync();
	if (progress->progressStatus() == Progress::SYNCING) {
		statusLabel->setText(tr("Sync in process"));
		progressIndicator->setRange(0, progress->progressEnd());
		progressIndicator->setValue(progress->progress());
		progressIndicator->setVisible(true);
	} else {
		statusLabel->setText(tr("Last sync: %1").arg(progress->getLastSyncFormated()));
		progressIndicator->setVisible(false);
	}
	qDebug() << "Progress status: " << progress->progressStatus();
}
