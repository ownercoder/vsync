/*
 * Copyright (c) 2011-2014 BlackBerry Limited.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ApplicationUI_HPP_
#define ApplicationUI_HPP_

#include <QObject>
#include <bb/cascades/Application>
#include <bb/cascades/Container>
#include <bb/cascades/NavigationPane>
#include <bb/system/InvokeManager>
#include <VBerry/ResponseAlbums.hpp>
#include <tc/utils/ConnectionInfo.hpp>
#include <bb/system/SystemUiResult>
#include <bb/cascades/LocaleHandler>

using namespace bb::cascades;
using namespace VBerry;
using namespace tc::utils;

class ApplicationUI : public QObject
{
    Q_OBJECT
public:
    NavigationPane* nav;
    static ApplicationUI* instance();
    bool isConnected();
    void showNoNetworkAvailableDialog();

    static const QString APP_NAME;
    static const QString APP_AUTHOR;
protected Q_SLOTS:
	void onSystemLanguageChanged();
	void onAuthCompleated();
	void onPopTransitionEnded (bb::cascades::Page *page);
	void onConnectionStateChanged(bool connected);
	void onShowNoNetworkAvailableDialogFinished(bb::system::SystemUiResult::Type type);
	void openMainWindow();
	void openEnterPage();
private:
	ApplicationUI(QObject *parent = 0);
    ConnectionInfo *connectionInfo;
    static ApplicationUI* appInstance;
    bb::system::InvokeManager* m_invokeManager;
    bool isNetworkConnected;
    QTranslator* m_translator;
	bb::cascades::LocaleHandler* m_localeHandler;
};

#endif /* ApplicationUI_HPP_ */
