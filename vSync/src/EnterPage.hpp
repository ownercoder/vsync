/*
 * EnterPage.hpp
 *
 *  Created on: 23 нояб. 2014 г.
 *      Author: sergey
 */

#ifndef ENTERPAGE_HPP_
#define ENTERPAGE_HPP_
#include <QObject>
#include <bb/cascades/Page>
#include <VBerry/OAuthConnection.h>
#include <bb/system/SystemUiResult>

using namespace VBerry;
using namespace bb::cascades;

class EnterPage: public Page {
	Q_OBJECT
public:
	EnterPage();
signals:
	void authCompleated();
protected Q_SLOTS:
	void onSignInClicked();
	void onSignUpClicked();
	void onUserActionRequired(bb::cascades::Page * page);
	void onAuthSuccess();
	void onErrorDetected(const QString &error, const QString &errorDescription);
	void onAccessDeniedDialog(bb::system::SystemUiResult::Type type);
private:
	OAuthConnection *oauthConnection;
};

#endif /* ENTERPAGE_HPP_ */
