/*
 * MainWindow.h
 *
 *  Created on: 08 нояб. 2014 г.
 *      Author: sergey
 */

#ifndef MAINWINDOW_H_
#define MAINWINDOW_H_

#include <QObject>
#include <bb/cascades/Page>
#include <bb/system/SystemProgressToast>
#include <bb/system/SystemUiProgressState>
#include <bb/system/SystemUiPosition>
#include <bb/cascades/DropDown>
#include <VBerry/RequestAlbums.hpp>
#include <VBerry/ResponseFriend.hpp>
#include <VBerry/Progress.hpp>
#include <bb/cascades/ActionItem>
#include <bb/cascades/Label>
#include <bb/cascades/ProgressIndicator>
#include <bb/system/InvokeManager>
#include <Settings.h>

using namespace bb::cascades;
using namespace bb::system;
using namespace VBerry;

class MainWindow : public Page {
	Q_OBJECT
public:
	MainWindow();
	Q_INVOKABLE void setValue(const QString &key, const QString &value);
	Q_INVOKABLE QString getValue(const QString &key);
	Q_INVOKABLE void showProgressToast(const QString &message, const QString &cancelTitle);
	Q_INVOKABLE void fillAblums();
public slots:
	void onConnectionStateChanged(bool state);
protected Q_SLOTS:
	void onToastEnabledChanged(bool changed);
	void onAlbumsReceived(const QList<ResponseAlbums*> & albums);
	void onInterfaceNameChanged(QString interface);
	void onUserInfoResponse(ResponseFriend* user);
	void getUserInfo();
	void onExitTriggered();
	void onActionPauseTriggered();
	void onActionSyncTriggered();
	void updateProgress();
	void onResponseError(int code);
protected:
	Settings::SyncActionState currentState;
	void setTitle(ResponseFriend* user);
	void createApplicationMenu();
	void createPageMenu();
	void updatePauseAction();
	QString saveFileName(const QUrl &url);

	bb::system::InvokeManager* m_invokeManager;
	TitleBar *pageTitleBar;
	RequestAlbums * requestAlbums;
	SystemProgressToast *progressToast;
	DropDown *dropDownAlbum;
	Label *statusLabel;
	ProgressIndicator *progressIndicator;
	ActionItem *itemPause;
	ActionItem *itemSync;
	Progress *progress;

};

#endif /* MAINWINDOW_H_ */
