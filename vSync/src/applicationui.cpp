/*
 * Copyright (c) 2011-2014 BlackBerry Limited.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "applicationui.hpp"

#include <bb/cascades/Application>
#include <bb/cascades/QmlDocument>
#include <bb/cascades/AbstractPane>
#include <bb/cascades/LocaleHandler>
#include <bb/system/InvokeManager>
#include <src/Settings.h>
#include "MainWindow.h"
#include <EnterPage.hpp>
#include <bb/system/SystemDialog>

using namespace bb::system;

ApplicationUI *ApplicationUI::appInstance = 0;

const QString ApplicationUI::APP_NAME = "vSyncHeadlessService";
const QString ApplicationUI::APP_AUTHOR = "Slytech";

ApplicationUI::ApplicationUI(QObject *parent)
		: QObject(parent),
		  m_translator(new QTranslator(this)),
		  m_localeHandler(new LocaleHandler(this)) {

	if (!QObject::connect(m_localeHandler, SIGNAL(systemLanguageChanged()),
			this, SLOT(onSystemLanguageChanged()))) {
		qDebug() << "Cannot load langs";
	}

	// initial load
	onSystemLanguageChanged();
	isNetworkConnected = false;

	m_invokeManager = new InvokeManager();

	connectionInfo = new ConnectionInfo();
	connect(connectionInfo, SIGNAL(connectedChanged(bool)), this, SLOT(onConnectionStateChanged(bool)));

	bb::system::InvokeRequest request;
	request.setTarget("com.slytech.vSyncHeadlessService");
	request.setAction("com.slytech.vSyncHeadlessService.START");
	m_invokeManager->invoke(request);

	nav = new NavigationPane();
	connect(nav, SIGNAL(popTransitionEnded(bb::cascades::Page*)), this, SLOT(onPopTransitionEnded(bb::cascades::Page*)));
	openEnterPage();
	if (Settings::instance()->token().length() > 0) {
		openMainWindow();
	}

	Application::instance()->setScene(nav);
}

ApplicationUI* ApplicationUI::instance() {
	if (!appInstance) {
		appInstance = new ApplicationUI();
	}

	return appInstance;
}

bool ApplicationUI::isConnected() {
	return isNetworkConnected;
}

void ApplicationUI::onConnectionStateChanged(bool connected) {
	qDebug() << connectionInfo->interfaceName();
	isNetworkConnected = connected;
}

void ApplicationUI::showNoNetworkAvailableDialog() {
	SystemDialog *dialog = new SystemDialog("Connect",
											"Cancel");

	dialog->setTitle("Offline");

	dialog->setBody("Phone is not connected to the Internet. Connect to the Internet and try again.");

	dialog->setEmoticonsEnabled(true);

	// Connect the finished() signal to the onDialogFinished() slot.
	// The slot will check the SystemUiResult to see which
	// button was tapped.
	bool success = connect(dialog,
		SIGNAL(finished(bb::system::SystemUiResult::Type)),
		this,
		SLOT(onShowNoNetworkAvailableDialogFinished(bb::system::SystemUiResult::Type)));

	if (success) {
		dialog->show();
	} else {
		dialog->deleteLater();
	}
}

void ApplicationUI::openMainWindow() {
	nav->setBackButtonsVisible(false);
	MainWindow* mainWindow = new MainWindow();
	connect(connectionInfo, SIGNAL(connectedChanged(bool)), mainWindow, SLOT(onConnectionStateChanged(bool)));
	mainWindow->onConnectionStateChanged(isConnected());

	nav->push(mainWindow);
}

void ApplicationUI::openEnterPage() {
	Application::instance()->setMenuEnabled(false);
	EnterPage *enterPage = new EnterPage();

	bool ok = connect(enterPage, SIGNAL(authCompleated()), this, SLOT(onAuthCompleated()));
	Q_ASSERT(ok);

	nav->insert(-1, enterPage);
}

void ApplicationUI::onAuthCompleated() {
	Page *currentPage = nav->top();
	openMainWindow();
	nav->remove(currentPage);
}

void ApplicationUI::onShowNoNetworkAvailableDialogFinished(
		bb::system::SystemUiResult::Type type) {

	if (type == bb::system::SystemUiResult::ConfirmButtonSelection) {
		InvokeManager invokeManager;
		InvokeRequest request;

		// Set the target app
		request.setTarget("sys.settings.card");

		// Set the action that the target app should execute
		request.setAction("bb.action.OPEN");

		// Set the MIME type of the data
		request.setMimeType("settings/view");

		// Specify the location of the data
		request.setUri(QUrl("settings://networkconnections"));

		invokeManager.invoke(request);
	}
}

void ApplicationUI::onPopTransitionEnded(bb::cascades::Page* page) {
	page->deleteLater();
	if (nav->count() == 1) {
		Application::instance()->setMenuEnabled(false);
	}
}

void ApplicationUI::onSystemLanguageChanged() {
	QCoreApplication::instance()->removeTranslator(m_translator);
	// Initiate, load and install the application translation files.
	QString locale_string = QLocale().name();
	QString file_name = QString("vSync_%1").arg(locale_string);
	if (m_translator->load(file_name, "app/native/qm")) {
		QCoreApplication::instance()->installTranslator(m_translator);
	}
}
