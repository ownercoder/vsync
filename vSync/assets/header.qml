import bb.cascades 1.2

Container {
    horizontalAlignment: HorizontalAlignment.Fill
    verticalAlignment: VerticalAlignment.Fill
    layout: StackLayout {
        orientation: LayoutOrientation.LeftToRight
    }
    ImageView {
      objectName: "userPic"
      id: userPic
      scalingMethod: ScalingMethod.AspectFit
      loadEffect: ImageViewLoadEffect.None
    }
    Container {
        layout: AbsoluteLayout { }
        leftMargin: 20
        verticalAlignment: VerticalAlignment.Center
        horizontalAlignment: HorizontalAlignment.Fill
        Label {
            layoutProperties: AbsoluteLayoutProperties { }
            objectName: "userName"
            textStyle {
                 base: SystemDefaults.TextStyles.PrimaryText
            }
        }
        Label {
            objectName: "userId"
            textStyle {
                base: SystemDefaults.TextStyles.SubtitleText
            }
            layoutProperties: AbsoluteLayoutProperties {
                positionY: 50
            }
        }
    }
    attachedObjects: [
        LayoutUpdateHandler {
            id: handler
            onLayoutFrameChanged: {
                userPic.preferredHeight = layoutFrame.height;
            }
        }
    ]
}
