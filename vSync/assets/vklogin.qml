import bb.cascades 1.2

Container {
    layout: DockLayout {

    }
    background: Color.create("#f5f5f5")
    ScrollView {
        scrollViewProperties.overScrollEffectMode: OverScrollEffectMode.OnPinch
        Container {
             layout: DockLayout {
        
             }
             WebView {
                 objectName: "loginWebView"
                 id: loginWebView
                 onLoadProgressChanged: {
                     progressIndicator.value = loadProgress;
                 }
                 onLoadingChanged: {
                     if (loadRequest.status == WebLoadStatus.Started) {
                         progressIndicator.visible = true;
                     }
                     if (loadRequest.status == WebLoadStatus.Succeeded) {
                         progressIndicator.visible = false;
                         progressIndicator.value = 0;
                     }
                 }
             }
         }
    }
    ProgressIndicator {
        visible: true
        id: progressIndicator
        horizontalAlignment: HorizontalAlignment.Fill
        state: ProgressIndicatorState.Progress
        toValue: 100.0
        value: 0.0
        verticalAlignment: VerticalAlignment.Bottom
        implicitLayoutAnimationsEnabled: false
    }
}