/*
 * Copyright (c) 2011-2014 BlackBerry Limited.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import bb.cascades 1.2
ScrollView {
     scrollViewProperties.initialScalingMethod: ScalingMethod.AspectFit
     scrollViewProperties.overScrollEffectMode: OverScrollEffectMode.OnPinchAndScroll
     horizontalAlignment: HorizontalAlignment.Fill
     scrollViewProperties {
         scrollMode: ScrollMode.Vertical
     }
     Container {
         id: mainContainer
         layout: StackLayout {
         
         }
         Container {
             horizontalAlignment: HorizontalAlignment.Fill
             topPadding: 35.0
             bottomMargin: 35.0
             Label {
                 objectName: "statusLabel"
                 text: qsTr("")
                 horizontalAlignment: HorizontalAlignment.Center
             }
             ProgressIndicator {
                 objectName: "progressIndicator"
                 horizontalAlignment: HorizontalAlignment.Fill
                 visible: false
             
             }
         }
         Header {
             title: qsTr("Sync params")
         }
         Container {
             topPadding: 15.0
             leftPadding: 15.0
             rightPadding: 15.0
             horizontalAlignment: HorizontalAlignment.Center
             Label {
                 text: qsTr("Automatically synchronize the selected options.")
                 multiline: true
                 textStyle.fontSize: FontSize.XXSmall
             }
         }
         Divider {
         
         }
         Container {
             layout: StackLayout {
                 orientation: LayoutOrientation.LeftToRight
             }
             
             topPadding: 15.0
             leftPadding: 15.0
             rightPadding: 15.0
             Label {
                 id: labelToggleContacts
                 text: qsTr("Contacts")
                 layoutProperties: StackLayoutProperties {
                     spaceQuota: 3.0
                 }
             }
             ToggleButton {
                 id: toggleContacts
                 checked: _settings.syncContacts()
                 accessibility.labelledBy: [ labelToggleContacts ]
                 layoutProperties: StackLayoutProperties {
                     spaceQuota: 2.0
                 }
                 onCheckedChanged: {
                     if (toggleContacts.checked != _settings.syncContacts())
                         _settings.setSyncContacts(toggleContacts.checked);
                 }
             }
         }
         Divider {
         
         }
         Container {
             layout: StackLayout {
                 orientation: LayoutOrientation.LeftToRight
             }
             
             leftPadding: 15.0
             rightPadding: 15.0
             bottomPadding: 15.0
             Label {
                 id: labelToggleAudio
                 text: qsTr("Audio")
                 layoutProperties: StackLayoutProperties {
                     spaceQuota: 3.0
                 }
             }
             ToggleButton {
                 id: toggleAudio
                 checked: _settings.syncAudio()
                 accessibility.labelledBy: [ labelToggleAudio ]
                 layoutProperties: StackLayoutProperties {
                     spaceQuota: 2.0
                 }
                 onCheckedChanged: {
                     if (toggleAudio.checked != _settings.syncAudio())
                         _settings.setSyncAudio(toggleAudio.checked);
                     dropDownAlbum.enabled = toggleAudio.checked;
                 }
             }
         }
         Container {
             leftPadding: 15.0
             rightPadding: 15.0
             bottomPadding: 15.0
             DropDown {
                 id: dropDownAlbum
                 objectName: "dropDownAlbum"
                 title: qsTr("Select an album")
                 onSelectedValueChanged: {
                     if (selectedValue != undefined) {
                         _settings.setAlbumId(selectedValue);
                     }
                 }
                 onEnabledChanged: {
                     if (enabled) {
                         _requestAlbums.getAlbums();
                         _app.showProgressToast(qsTr("Receiving albums"), qsTr("Cancel"));
                     }
                 }
                 onCreationCompleted: {
                     dropDownAlbum.enabled = _settings.syncAudio();
                 }
                 selectedIndex: -1
             }
         }
         Header {
             title: qsTr("Network")
         }
         Container {
             topPadding: 15.0
             leftPadding: 15.0
             rightPadding: 15.0
             horizontalAlignment: HorizontalAlignment.Center
             Label {
                 text: qsTr("If the option is enabled, synchronization is performed by the mobile network. Synchronization in roaming is not executed. If you are concerned about the amount of transmitted data, this option should be disabled.")
                 multiline: true
                 textStyle.fontSize: FontSize.XXSmall
             }
         }
         Divider {
         
         }
         Container {
             layout: StackLayout {
                 orientation: LayoutOrientation.LeftToRight
             }
             
             leftPadding: 15.0
             rightPadding: 15.0
             bottomPadding: 15.0
             Label {
                 id: labelToggleMobileNetwork
                 text: qsTr("Use mobile network")
                 layoutProperties: StackLayoutProperties {
                     spaceQuota: 3.0
                 }
             }
             ToggleButton {
                 id: toggleMobileData
                 checked: _settings.syncViaMobileData()
                 accessibility.labelledBy: [ labelToggleMobileNetwork ]
                 layoutProperties: StackLayoutProperties {
                     spaceQuota: 2.0
                 }
                 onCheckedChanged: {
                     if (toggleMobileData.checked != _settings.syncViaMobileData())
                         _settings.setSyncViaMobileData(toggleMobileData.checked);
                 }
             }
         }
     }
}
