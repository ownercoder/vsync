import bb.cascades 1.2
Container {
    background: Color.White
    layout: DockLayout {

    }
    verticalAlignment: VerticalAlignment.Fill
    horizontalAlignment: HorizontalAlignment.Fill
    Container {
        bottomPadding: 100
        layout: StackLayout {
        
        }
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Top
        Container {
            horizontalAlignment: HorizontalAlignment.Center
            verticalAlignment: VerticalAlignment.Center
            ImageView {
                imageSource: "asset:///images/icon.png"
                horizontalAlignment: HorizontalAlignment.Center
                scalingMethod: ScalingMethod.AspectFit
                loadEffect: ImageViewLoadEffect.None
                verticalAlignment: VerticalAlignment.Top
                implicitLayoutAnimationsEnabled: false
            }
        }
        Container {
            horizontalAlignment: HorizontalAlignment.Center
            verticalAlignment: VerticalAlignment.Center
            layout: StackLayout {
            
            }
            Button {
                text: qsTr("Sign in")
                horizontalAlignment: HorizontalAlignment.Center
                objectName: "signinButton"
                id: signinButton
                verticalAlignment: VerticalAlignment.Center
            }
            Button {
                text: qsTr("Sign up")
                horizontalAlignment: HorizontalAlignment.Center
                objectName: "signupButton"
                id: signupButton
                verticalAlignment: VerticalAlignment.Center
            }
        }
        /*Container {
         horizontalAlignment: HorizontalAlignment.Center
         verticalAlignment: VerticalAlignment.Center
         Label {
         text: qsTr("ℹ Getting Started Guide")
         }
         }*/
    }
    Container {
         horizontalAlignment: HorizontalAlignment.Center
         verticalAlignment: VerticalAlignment.Bottom
         bottomPadding: 20.0
         Label {
             text: qsTr("&copy; 2014 vSync")
             textFormat: TextFormat.Html
             textStyle.fontSize: FontSize.Small
             textStyle.color: Color.create("#005895")
         }
    }
}