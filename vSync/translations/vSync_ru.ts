<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ru_RU">
<context>
    <name>Copy of main</name>
    <message>
        <location filename="../assets/Copy of main.qml" line="86"/>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/Copy of main.qml" line="97"/>
        <source>Sync params</source>
        <translation type="unfinished">Параметры синхронизации</translation>
    </message>
    <message>
        <location filename="../assets/Copy of main.qml" line="105"/>
        <source>Automatically synchronize the selected options.</source>
        <translation type="unfinished">Автоматически синхронизировать выбраные опции.</translation>
    </message>
    <message>
        <location filename="../assets/Copy of main.qml" line="123"/>
        <source>Contacts</source>
        <translation type="unfinished">Контакты</translation>
    </message>
    <message>
        <location filename="../assets/Copy of main.qml" line="154"/>
        <source>Audio</source>
        <translation type="unfinished">Аудио</translation>
    </message>
    <message>
        <location filename="../assets/Copy of main.qml" line="180"/>
        <source>Select an album</source>
        <translation type="unfinished">Выбрать альбом</translation>
    </message>
    <message>
        <location filename="../assets/Copy of main.qml" line="189"/>
        <source>Receiving albums</source>
        <translation type="unfinished">Получение списка альбомов</translation>
    </message>
    <message>
        <location filename="../assets/Copy of main.qml" line="189"/>
        <source>Cancel</source>
        <translation type="unfinished">Отменить</translation>
    </message>
    <message>
        <location filename="../assets/Copy of main.qml" line="199"/>
        <source>Network</source>
        <translation type="unfinished">Подключение</translation>
    </message>
    <message>
        <location filename="../assets/Copy of main.qml" line="207"/>
        <source>If the option is enabled, synchronization is performed by the mobile network. Synchronization in roaming is not executed. If you are concerned about the amount of transmitted data, this option should be disabled.</source>
        <translation type="unfinished">Если эта опция включена, синхронизация будет выполняться через мобильную сеть. Синхронизация в роуминге не выполняется. Если вы беспокоитесь о размере передаваемых данных, эта опция должна быть отключена.  </translation>
    </message>
    <message>
        <location filename="../assets/Copy of main.qml" line="225"/>
        <source>Use mobile network</source>
        <translation type="unfinished">Использовать мобильную сеть</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/MainWindow.cpp" line="88"/>
        <source>Exit</source>
        <translation>Выход</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="217"/>
        <location filename="../src/MainWindow.cpp" line="254"/>
        <source>Pause</source>
        <translation>Приостановить</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="218"/>
        <source>Sync</source>
        <translation>Синхронизировать</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="259"/>
        <source>Continue</source>
        <translation>Прожолжить</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="299"/>
        <source>Sync in process</source>
        <translation>Синхронизация в процессе</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="304"/>
        <source>Last sync: %1</source>
        <translation>Последняя синхронизация: %1</translation>
    </message>
</context>
<context>
    <name>VBerry::Progress</name>
    <message>
        <location filename="../src/VBerry/Progress.cpp" line="45"/>
        <source>never</source>
        <translation>никогда</translation>
    </message>
    <message>
        <location filename="../src/VBerry/Progress.cpp" line="49"/>
        <source>just now</source>
        <translation>только что</translation>
    </message>
    <message>
        <location filename="../src/VBerry/Progress.cpp" line="51"/>
        <source>%1 minute(s) ago</source>
        <translation>%1 минут(ы) назад</translation>
    </message>
    <message>
        <location filename="../src/VBerry/Progress.cpp" line="53"/>
        <source>%1 hour(s) ago</source>
        <translation>%1 час(а) назад</translation>
    </message>
    <message>
        <location filename="../src/VBerry/Progress.cpp" line="55"/>
        <source>%1 day(s) ago</source>
        <translation>%1 день назад</translation>
    </message>
</context>
<context>
    <name>enter</name>
    <message>
        <location filename="../assets/enter.qml" line="35"/>
        <source>Sign in</source>
        <translation>Войти</translation>
    </message>
    <message>
        <location filename="../assets/enter.qml" line="42"/>
        <source>Sign up</source>
        <translation>Регистрация</translation>
    </message>
    <message>
        <location filename="../assets/enter.qml" line="62"/>
        <source>&amp;copy; 2014 vSync</source>
        <translation>&amp;copy; 2014 vSync</translation>
    </message>
</context>
<context>
    <name>header</name>
    <message>
        <location filename="../assets/header.qml" line="25"/>
        <source></source>
        <translation></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../assets/main.qml" line="29"/>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="46"/>
        <source>Sync params</source>
        <translation>Параметры синхронизации</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="54"/>
        <source>Automatically synchronize the selected options.</source>
        <translation>Автоматически синхронизировать выбраные опции.</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="72"/>
        <source>Contacts</source>
        <translation>Контакты</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="103"/>
        <source>Audio</source>
        <translation>Аудио</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="129"/>
        <source>Select an album</source>
        <translation>Выбрать альбом</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="138"/>
        <source>Receiving albums</source>
        <translation>Получение списка альбомов</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="138"/>
        <source>Cancel</source>
        <translation>Отменить</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="148"/>
        <source>Network</source>
        <translation>Подключение</translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="156"/>
        <source>If the option is enabled, synchronization is performed by the mobile network. Synchronization in roaming is not executed. If you are concerned about the amount of transmitted data, this option should be disabled.</source>
        <translation>Если эта опция включена, синхронизация будет выполняться через мобильную сеть. Синхронизация в роуминге не выполняется. Если вы беспокоитесь о размере передаваемых данных, эта опция должна быть отключена.  </translation>
    </message>
    <message>
        <source>If the option is enabled, synchronization is performed by the mobile network. Synchronization files roaming is not executed. If you are concerned about the amount of transmitted data, this option should be disabled.</source>
        <translation type="obsolete">Если эта опция включена, синхронизация будет выполняться через мобильную сеть. Синхронизация в роуминге не выполняется. Если вы беспокоитесь о размере передаваемых данных, эта опция должна быть отключена.  </translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="174"/>
        <source>Use mobile network</source>
        <translation>Использовать мобильную сеть</translation>
    </message>
</context>
</TS>
