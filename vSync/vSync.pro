APP_NAME = vSync

CONFIG += qt warn_on cascades10

INCLUDEPATH += $$PWD/src

QT += core network
LIBS += -lbbdata -lbb -lbbsystem -lbbplatform -lbbpim

include(config.pri)
